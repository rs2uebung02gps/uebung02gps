`include "axiinterface.vh"

module WriteControl #
(
	parameter integer MAX_ID_WIDTH = -1, // the width directly taken from the address
	parameter integer OTHER_ID_WIDTH = -1, // the width needed for all IDs of "other" targets
	parameter integer PE_ID_WIDTH = -1 // ...
)
(
input wire CGRA_CLK,
input wire EN_I,
input wire RST_N_I,
input wire EN_CONTROLLER_I,
input wire VALID_CONTEXT_I,
input wire [`OP_WIDTH-1:0] OPERATION_I, // specifies what is to be done: write contexts/locals ...
input wire [MAX_ID_WIDTH-1:0] TRANSACTION_ID_I, // specifies the statically assigned ID of "other" modules (CCU, CBox, ...) and PEs
input wire PARAMETER_UPDATE_ALLOWED_I, // CGRA FSM signal informing this module to write parameters
input wire [PE_ID_WIDTH-1:0] PE_ID_I, // the ID coming from the parameter buffer
// control
output wire IS_PELOG_CONTEXT_O, // treat all PE enables as PE log context enables						
output wire CONTEXT_WREN_PE0_O,
output wire PARAMETER_CONTEXT_WREN_PE0_O,
output wire CONTEXT_WREN_PE1_O,
output wire PARAMETER_CONTEXT_WREN_PE1_O,
output wire CONTEXT_WREN_PE2_O,
output wire PARAMETER_CONTEXT_WREN_PE2_O,
output wire CONTEXT_WREN_PE3_O,
output wire PARAMETER_CONTEXT_WREN_PE3_O,

output wire CONTEXT_WREN_CCU_O,
output wire CONTEXT_WREN_CBOX_O,
output wire CONTEXT_WREN_IDC_O,
output wire CONTEXT_WREN_GLOG_O,
output wire CONTEXT_WREN_ACTOR_O,
output wire CONTEXT_WREN_SENSOR_O,
output wire PARAMETER_BUFFER_WREN_O,
output wire SYNCUNIT_STATE_CHANGE_O,
output wire SYNCUNIT_INTERVAL_CHANGE_O,
output wire LOG_DEST_CHANGE_O,
output wire LOG_DEST_BOUND_CHANGE_O,
output wire LOG_DEST_INC_CHANGE_O,
output wire OCM_DEST_CHANGE_O,
output wire OCM_DEST_BOUND_CHANGE_O,
output wire OCM_DEST_INC_CHANGE_O,
output wire CONTEXT_WREN_OCM_O,
output wire CONST_BUF_WREN_O,
output wire PREPARE_PARAMETER_CONTEXT_O,	// informs the CGRA FSM that it should change its state to handle a parameter write
output wire PE_CONTEXT_ENABLE_O	// informs the CGRA that some kind of PE context will be written 
);

reg parameterContextWrEn_pe0;
reg parameterContextWrEn_pe0_sync;
assign PARAMETER_CONTEXT_WREN_PE0_O = parameterContextWrEn_pe0_sync;
reg parameterContextWrEn_pe1;
reg parameterContextWrEn_pe1_sync;
assign PARAMETER_CONTEXT_WREN_PE1_O = parameterContextWrEn_pe1_sync;
reg parameterContextWrEn_pe2;
reg parameterContextWrEn_pe2_sync;
assign PARAMETER_CONTEXT_WREN_PE2_O = parameterContextWrEn_pe2_sync;
reg parameterContextWrEn_pe3;
reg parameterContextWrEn_pe3_sync;
assign PARAMETER_CONTEXT_WREN_PE3_O = parameterContextWrEn_pe3_sync;

reg prepareParameterContext;
reg prepareParameterContext_sync;
assign PREPARE_PARAMETER_CONTEXT_O = prepareParameterContext_sync;

/* This block handles writing parameters from the buffer
 * to their final destination */ 
always @(*) begin
	parameterContextWrEn_pe0 = 1'b0;
	parameterContextWrEn_pe1 = 1'b0;
	parameterContextWrEn_pe2 = 1'b0;
	parameterContextWrEn_pe3 = 1'b0;

	prepareParameterContext = 1'b0;

	if (PARAMETER_UPDATE_ALLOWED_I) begin
		case(PE_ID_I)
			`ID_PE0: parameterContextWrEn_pe0 = 1'b1;
			`ID_PE1: parameterContextWrEn_pe1 = 1'b1;
			`ID_PE2: parameterContextWrEn_pe2 = 1'b1;
			`ID_PE3: parameterContextWrEn_pe3 = 1'b1;
 		
		endcase
		prepareParameterContext = 1'b1;
	end
end

always @(posedge CGRA_CLK) begin
	if (~RST_N_I) begin
		parameterContextWrEn_pe0_sync <= 1'b0;
		parameterContextWrEn_pe1_sync <= 1'b0;
		parameterContextWrEn_pe2_sync <= 1'b0;
		parameterContextWrEn_pe3_sync <= 1'b0;

		prepareParameterContext_sync <= 1'b0;
	end else if (EN_I) begin
		parameterContextWrEn_pe0_sync <= parameterContextWrEn_pe0;
		parameterContextWrEn_pe1_sync <= parameterContextWrEn_pe1;
		parameterContextWrEn_pe2_sync <= parameterContextWrEn_pe2;
		parameterContextWrEn_pe3_sync <= parameterContextWrEn_pe3;
		
		prepareParameterContext_sync <= prepareParameterContext;
	end
end

reg contextWrEn_pe0;
reg contextWrEn_pe0_sync;
assign CONTEXT_WREN_PE0_O = contextWrEn_pe0_sync;
reg contextWrEn_pe1;
reg contextWrEn_pe1_sync;
assign CONTEXT_WREN_PE1_O = contextWrEn_pe1_sync;
reg contextWrEn_pe2;
reg contextWrEn_pe2_sync;
assign CONTEXT_WREN_PE2_O = contextWrEn_pe2_sync;
reg contextWrEn_pe3;
reg contextWrEn_pe3_sync;
assign CONTEXT_WREN_PE3_O = contextWrEn_pe3_sync;

reg isPELogContext;
reg contextWrEn_ccu;
reg contextWrEn_cbox;
reg contextWrEn_idc;
reg contextWrEn_glblLog;
reg contextWrEn_sensor;
reg contextWrEn_actor;
reg parameterBufferWrEn;
reg syncUnitStateChange;
reg syncUnitIntervalChange;
reg logDestWrEn;
reg logDestBoundWrEn;
reg logDestIncWrEn;
reg ocmDestWrEn;
reg ocmDestBoundWrEn;
reg ocmDestIncWrEn;
reg ocmContextWrEn;
reg constBufferWrEn;
reg peContextEnable;

reg isPELogContext_sync;
reg contextWrEn_ccu_sync;
reg contextWrEn_cbox_sync;
reg contextWrEn_idc_sync;
reg contextWrEn_glblLog_sync;
reg contextWrEn_sensor_sync;
reg contextWrEn_actor_sync;
reg parameterBufferWrEn_sync;
reg syncUnitStateChange_sync;
reg syncUnitIntervalChange_sync;
reg logDestWrEn_sync;
reg logDestBoundWrEn_sync;
reg logDestIncWrEn_sync;
reg ocmDestWrEn_sync;
reg ocmDestBoundWrEn_sync;
reg ocmDestIncWrEn_sync;
reg ocmContextWrEn_sync;
reg constBufferWrEn_sync;
reg peContextEnable_sync;

assign IS_PELOG_CONTEXT_O = isPELogContext_sync;
assign CONTEXT_WREN_CCU_O = contextWrEn_ccu_sync;
assign CONTEXT_WREN_CBOX_O = contextWrEn_cbox_sync;
assign CONTEXT_WREN_IDC_O = contextWrEn_idc_sync;
assign CONTEXT_WREN_GLOG_O = contextWrEn_glblLog_sync;
assign CONTEXT_WREN_ACTOR_O = contextWrEn_sensor_sync;
assign CONTEXT_WREN_SENSOR_O = contextWrEn_actor_sync;
assign PARAMETER_BUFFER_WREN_O = parameterBufferWrEn_sync;
assign SYNCUNIT_STATE_CHANGE_O = syncUnitStateChange_sync;
assign SYNCUNIT_INTERVAL_CHANGE_O = syncUnitIntervalChange_sync;
assign LOG_DEST_CHANGE_O = logDestWrEn_sync;
assign LOG_DEST_BOUND_CHANGE_O = logDestBoundWrEn_sync;
assign LOG_DEST_INC_CHANGE_O = logDestIncWrEn_sync;
assign OCM_DEST_CHANGE_O = ocmDestWrEn_sync;
assign OCM_DEST_BOUND_CHANGE_O = ocmDestBoundWrEn_sync;
assign OCM_DEST_INC_CHANGE_O = ocmDestIncWrEn_sync;
assign CONTEXT_WREN_OCM_O = ocmContextWrEn_sync;
assign CONST_BUF_WREN_O = constBufferWrEn_sync;
assign PE_CONTEXT_ENABLE_O = peContextEnable_sync;

/* This block handles all AXI interface operations,
 * writing data to contexts or buffers. */
always @(*) begin
	isPELogContext = 1'b0;
	contextWrEn_pe0 = 1'b0;
	contextWrEn_pe1 = 1'b0;
	contextWrEn_pe2 = 1'b0;
	contextWrEn_pe3 = 1'b0;

	contextWrEn_ccu = 1'b0;
	contextWrEn_cbox = 1'b0;
	contextWrEn_idc = 1'b0;
	contextWrEn_glblLog = 1'b0;
	contextWrEn_sensor = 1'b0;
	contextWrEn_actor = 1'b0;
	parameterBufferWrEn = 1'b0;
	syncUnitStateChange = 1'b0;
	syncUnitIntervalChange = 1'b0;
	logDestWrEn = 1'b0;
	logDestBoundWrEn = 1'b0;
	logDestIncWrEn = 1'b0;
	ocmDestWrEn = 1'b0;
	ocmDestBoundWrEn = 1'b0;
	ocmDestIncWrEn = 1'b0;
	ocmContextWrEn = 1'b0;
	constBufferWrEn	 = 1'b0;
	peContextEnable = 1'b0;

	if (EN_CONTROLLER_I) begin
		case(OPERATION_I)
			`WRITE_CONTEXT_OTHER: begin
				case(TRANSACTION_ID_I[OTHER_ID_WIDTH-1:0])
					`ID_CCU: contextWrEn_ccu = VALID_CONTEXT_I;
					`ID_CBOX: contextWrEn_cbox = VALID_CONTEXT_I;
					`ID_IDC: contextWrEn_idc = VALID_CONTEXT_I;
					`ID_GLOG: contextWrEn_glblLog = VALID_CONTEXT_I; 
					`ID_ACTOR: contextWrEn_sensor = VALID_CONTEXT_I;
					`ID_SENSOR: contextWrEn_actor = VALID_CONTEXT_I;
					`ID_INTERVAL: syncUnitIntervalChange = VALID_CONTEXT_I;
					`ID_LOG_DEST: logDestWrEn = VALID_CONTEXT_I;
					`ID_LOG_DEST_BOUND: logDestBoundWrEn = VALID_CONTEXT_I;
					`ID_LOG_DEST_INC: logDestIncWrEn = VALID_CONTEXT_I;
					`ID_OCM_DEST: ocmDestWrEn = VALID_CONTEXT_I;
					`ID_OCM_DEST_BOUND: ocmDestBoundWrEn = VALID_CONTEXT_I;
					`ID_OCM_DEST_INC: ocmDestIncWrEn = VALID_CONTEXT_I;
					`ID_OCM_CONTEXT: ocmContextWrEn = VALID_CONTEXT_I;
					`ID_CONST_BUF: constBufferWrEn = VALID_CONTEXT_I;
				endcase
			end
			`WRITE_CONTEXT_PE: begin
				case(TRANSACTION_ID_I[PE_ID_WIDTH-1:0])
					`ID_PE0: contextWrEn_pe0 = VALID_CONTEXT_I;
					`ID_PE1: contextWrEn_pe1 = VALID_CONTEXT_I;
					`ID_PE2: contextWrEn_pe2 = VALID_CONTEXT_I;
					`ID_PE3: contextWrEn_pe3 = VALID_CONTEXT_I;
				
				endcase
				isPELogContext = TRANSACTION_ID_I[MAX_ID_WIDTH-1];
				if (~isPELogContext)
					peContextEnable = 1'b1;
			end
			`HOST_STATE_CHANGE: begin
				syncUnitStateChange = 1'b1;
			end
			`WRITE_PARAMETER: begin
				parameterBufferWrEn = 1'b1;
			end
		endcase
	end
end

always @(posedge CGRA_CLK) begin
	if (~RST_N_I) begin
		isPELogContext_sync <= 1'b0;
		contextWrEn_pe0_sync <= 1'b0;
		contextWrEn_pe1_sync <= 1'b0;
		contextWrEn_pe2_sync <= 1'b0;
		contextWrEn_pe3_sync <= 1'b0;
 		
		contextWrEn_ccu_sync <= 1'b0;
		contextWrEn_cbox_sync <= 1'b0;
		contextWrEn_idc_sync <= 1'b0;
		contextWrEn_glblLog_sync <= 1'b0;
		contextWrEn_sensor_sync <= 1'b0;
		contextWrEn_actor_sync <= 1'b0;
		parameterBufferWrEn_sync <= 1'b0;
		syncUnitStateChange_sync <= 1'b0;
		syncUnitIntervalChange_sync <= 1'b0;
		logDestWrEn_sync <= 1'b0;
		logDestBoundWrEn_sync <= 1'b0;
		logDestIncWrEn_sync <= 1'b0;
		ocmDestWrEn_sync <= 1'b0;
		ocmDestBoundWrEn_sync <= 1'b0;
		ocmDestIncWrEn_sync <= 1'b0;
		ocmContextWrEn_sync <= 1'b0;
		constBufferWrEn_sync <= 1'b0;		
		peContextEnable_sync <= 1'b0;
	end else if (EN_I) begin
		isPELogContext_sync <= isPELogContext;
		contextWrEn_pe0_sync <= contextWrEn_pe0;
		contextWrEn_pe1_sync <= contextWrEn_pe1;
		contextWrEn_pe2_sync <= contextWrEn_pe2;
		contextWrEn_pe3_sync <= contextWrEn_pe3;
		
		contextWrEn_ccu_sync <= contextWrEn_ccu;
		contextWrEn_cbox_sync <= contextWrEn_cbox;
		contextWrEn_idc_sync <= contextWrEn_idc;
		contextWrEn_glblLog_sync <= contextWrEn_glblLog;
		contextWrEn_sensor_sync <= contextWrEn_sensor;
		contextWrEn_actor_sync <= contextWrEn_actor;
		parameterBufferWrEn_sync <= parameterBufferWrEn;
		syncUnitStateChange_sync <= syncUnitStateChange;
		syncUnitIntervalChange_sync <= syncUnitIntervalChange;
		logDestWrEn_sync <= logDestWrEn;
		logDestBoundWrEn_sync <= logDestBoundWrEn;
		logDestIncWrEn_sync <= logDestIncWrEn;
		ocmDestWrEn_sync <= ocmDestWrEn;
		ocmDestBoundWrEn_sync <= ocmDestBoundWrEn;
		ocmDestIncWrEn_sync <= ocmDestIncWrEn;
		ocmContextWrEn_sync <= ocmContextWrEn;
		constBufferWrEn_sync <= constBufferWrEn;		
		peContextEnable_sync <= peContextEnable;
	end
end

endmodule

