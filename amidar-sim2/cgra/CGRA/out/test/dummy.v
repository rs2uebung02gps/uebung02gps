`include "axiinterface.vh"

module Dummy (
(* dont_touch = "true" *) input wire CLK_I,
(* dont_touch = "true" *) input wire S_AXI_ACLK_I,
(* dont_touch = "true" *) input wire EN_I,
(* dont_touch = "true" *) input wire RST_I,
(* dont_touch = "true" *) input wire [100-1:0] IN_I,
(* dont_touch = "true" *) output wire OUT_O
);

wire [8-1:0] S_AXI_ARLEN_I;
wire [`MASTER_DATA_WIDTH-1:0] M_LOG_WDATA_O;
wire S_AXI_BREADY_I;
wire S_AXI_AWREADY_O;
wire [3-1:0] M_LOG_AWSIZE_O;
wire M_LOG_WREADY_I;
wire S_AXI_RVALID_O;
wire M_LOG_AWREADY_I;
wire S_AXI_WLAST_I;
wire [`MASTER_DATA_WIDTH-1:0] M_OCM_WDATA_O;
wire M_OCM_BVALID_I;
wire S_AXI_WREADY_O;
wire S_AXI_WVALID_I;
wire M_LOG_WLAST_O;
wire [2-1:0] M_LOG_AWBURST_O;
wire [3-1:0] M_OCM_AWSIZE_O;
wire M_LOG_BVALID_I;
wire [4-1:0] S_AXI_WSTRB_I;
wire [2-1:0] M_OCM_AWBURST_O;
wire M_OCM_WVALID_O;
wire [3-1:0] S_AXI_ARSIZE_I;
wire M_OCM_AWVALID_O;
wire [2-1:0] S_AXI_RRESP_O;
wire [8-1:0] M_OCM_AWLEN_O;
wire S_AXI_ARREADY_O;
wire S_AXI_BVALID_O;
wire [8-1:0] M_LOG_AWLEN_O;
wire M_OCM_AWREADY_I;
wire [32-1:0] S_AXI_WDATA_I;
wire [32-1:0] M_LOG_AWADDR_O;
wire S_AXI_RLAST_O;
wire M_OCM_WREADY_I;
wire [32-1:0] S_AXI_ARADDR_I;
wire S_AXI_AWVALID_I;
wire M_OCM_WLAST_O;
wire S_AXI_RREADY_I;
wire [32-1:0] S_AXI_RDATA_O;
wire [32-1:0] M_OCM_AWADDR_O;
wire [`MASTER_DATA_WIDTH/8-1:0] M_LOG_WSTRB_O;
wire [2-1:0] M_OCM_BRESP_I;
wire [2-1:0] S_AXI_AWBURST_I;
wire SENSOR_SYNC_IN_O;
wire M_LOG_BREADY_O;
wire S_AXI_ARVALID_I;
wire ACTOR_SYNC_OUT_O;
wire [2-1:0] M_LOG_BRESP_I;
wire [8-1:0] S_AXI_AWLEN_I;
wire [2-1:0] S_AXI_ARBURST_I;
wire M_LOG_AWVALID_O;
wire M_LOG_WVALID_O;
wire [2-1:0] S_AXI_BRESP_O;
wire [3-1:0] S_AXI_AWSIZE_I;
wire [32-1:0] S_AXI_AWADDR_I;
wire SENSOR_DONE_I;
wire [`MASTER_DATA_WIDTH/8-1:0] M_OCM_WSTRB_O;
wire M_OCM_BREADY_O;
wire ERROR_O;
wire [32-1:0] SENSOR_DATA;
wire [32-1:0] ACTOR_DATA;
reg rst;

assign S_AXI_ARLEN_I = IN_I[8-1:0];
assign S_AXI_BREADY_I = IN_I[3];
assign M_LOG_WREADY_I = IN_I[6];
assign M_LOG_AWREADY_I = IN_I[7];
assign S_AXI_WLAST_I = IN_I[1];
assign M_OCM_BVALID_I = IN_I[0];
assign S_AXI_WVALID_I = IN_I[1];
assign M_LOG_BVALID_I = IN_I[2];
assign S_AXI_WSTRB_I = IN_I[4-1:0];
assign S_AXI_ARSIZE_I = IN_I[3-1:0];
assign M_OCM_AWREADY_I = IN_I[12];
assign S_AXI_WDATA_I = IN_I[32-1:0];
assign M_OCM_WREADY_I = IN_I[11];
assign S_AXI_ARADDR_I = IN_I[13];
assign S_AXI_AWVALID_I = IN_I[1];
assign S_AXI_RREADY_I = IN_I[13];
assign M_OCM_BRESP_I = IN_I[2-1:0];
assign S_AXI_AWBURST_I = IN_I[2-1:0];
assign S_AXI_ARVALID_I = IN_I[0];
assign M_LOG_BRESP_I = IN_I[2-1:0];
assign S_AXI_AWLEN_I = IN_I[8-1:0];
assign S_AXI_ARBURST_I = IN_I[2-1:0];
assign S_AXI_AWSIZE_I = IN_I[3-1:0];
assign S_AXI_AWADDR_I = IN_I[32-1:0];
assign SENSOR_DONE_I = IN_I[99];
assign SENSOR_DATA = IN_I[32-1:0];

assign OUT_O = 
M_LOG_WDATA_O &
S_AXI_AWREADY_O &
M_LOG_AWSIZE_O &
S_AXI_RVALID_O &
M_OCM_WDATA_O &
S_AXI_WREADY_O &
M_LOG_WLAST_O &
M_LOG_AWBURST_O &
M_OCM_AWSIZE_O &
M_OCM_AWBURST_O &
M_OCM_WVALID_O &
M_OCM_AWVALID_O &
S_AXI_RRESP_O &
M_OCM_AWLEN_O &
S_AXI_ARREADY_O &
S_AXI_BVALID_O &
M_LOG_AWLEN_O &
M_LOG_AWADDR_O &
S_AXI_RLAST_O &
M_OCM_WLAST_O &
S_AXI_RDATA_O &
M_OCM_AWADDR_O &
M_LOG_WSTRB_O &
SENSOR_SYNC_IN_O &
M_LOG_BREADY_O &
ACTOR_SYNC_OUT_O &
M_LOG_AWVALID_O &
M_LOG_WVALID_O &
S_AXI_BRESP_O &
M_OCM_WSTRB_O &
M_OCM_BREADY_O &
ERROR_O &
ACTOR_DATA;

always @(posedge CLK_I) begin
	rst <= RST_I;
end


Cgra_Ultrasynth dummy_instance (
	.S_AXI_ARLEN_I(S_AXI_ARLEN_I),
	.EN_I(EN_I),
	.ERROR_O(ERROR_O),
	.M_LOG_WDATA_O(M_LOG_WDATA_O),
	.S_AXI_BREADY_I(S_AXI_BREADY_I),
	.S_AXI_AWREADY_O(S_AXI_AWREADY_O),
	.M_LOG_AWSIZE_O(M_LOG_AWSIZE_O),
	.M_LOG_WREADY_I(M_LOG_WREADY_I),
	.S_AXI_RVALID_O(S_AXI_RVALID_O),
	.M_LOG_AWREADY_I(M_LOG_AWREADY_I),
	.S_AXI_WLAST_I(S_AXI_WLAST_I),
	.M_OCM_WDATA_O(M_OCM_WDATA_O),
	.M_OCM_BVALID_I(M_OCM_BVALID_I),
	.S_AXI_WREADY_O(S_AXI_WREADY_O),
	.S_AXI_WVALID_I(S_AXI_WVALID_I),
	.M_LOG_WLAST_O(M_LOG_WLAST_O),
	.M_LOG_AWBURST_O(M_LOG_AWBURST_O),
	.M_OCM_AWSIZE_O(M_OCM_AWSIZE_O),
	.M_LOG_BVALID_I(M_LOG_BVALID_I),
	.S_AXI_WSTRB_I(S_AXI_WSTRB_I),
	.M_OCM_AWBURST_O(M_OCM_AWBURST_O),
	.M_OCM_WVALID_O(M_OCM_WVALID_O),
	.S_AXI_ARSIZE_I(S_AXI_ARSIZE_I),
	.M_OCM_AWVALID_O(M_OCM_AWVALID_O),
	.S_AXI_ACLK_I(S_AXI_ACLK_I),
	.S_AXI_RRESP_O(S_AXI_RRESP_O),
	.M_OCM_AWLEN_O(M_OCM_AWLEN_O),
	.S_AXI_ARREADY_O(S_AXI_ARREADY_O),
	.S_AXI_BVALID_O(S_AXI_BVALID_O),
	.M_LOG_AWLEN_O(M_LOG_AWLEN_O),
	.M_OCM_AWREADY_I(M_OCM_AWREADY_I),
	.S_AXI_WDATA_I(S_AXI_WDATA_I),
	.M_LOG_AWADDR_O(M_LOG_AWADDR_O),
	.S_AXI_RLAST_O(S_AXI_RLAST_O),
	.M_OCM_WREADY_I(M_OCM_WREADY_I),
	.S_AXI_ARADDR_I(S_AXI_ARADDR_I),
	.S_AXI_AWVALID_I(S_AXI_AWVALID_I),
	.M_OCM_WLAST_O(M_OCM_WLAST_O),
	.S_AXI_RREADY_I(S_AXI_RREADY_I),
	.RST_I(rst),
	.CLK_I(CLK_I),
	.S_AXI_RDATA_O(S_AXI_RDATA_O),
	.M_OCM_AWADDR_O(M_OCM_AWADDR_O),
	.M_LOG_WSTRB_O(M_LOG_WSTRB_O),
	.M_OCM_BRESP_I(M_OCM_BRESP_I),
	.S_AXI_AWBURST_I(S_AXI_AWBURST_I),
	.SENSOR_SYNC_IN_O(SENSOR_SYNC_IN_O),
	.M_LOG_BREADY_O(M_LOG_BREADY_O),
	.S_AXI_ARVALID_I(S_AXI_ARVALID_I),
	.ACTOR_SYNC_OUT_O(ACTOR_SYNC_OUT_O),
	.M_LOG_BRESP_I(M_LOG_BRESP_I),
	.S_AXI_AWLEN_I(S_AXI_AWLEN_I),
	.S_AXI_ARBURST_I(S_AXI_ARBURST_I),
	.M_LOG_AWVALID_O(M_LOG_AWVALID_O),
	.M_LOG_WVALID_O(M_LOG_WVALID_O),
	.S_AXI_BRESP_O(S_AXI_BRESP_O),
	.S_AXI_AWSIZE_I(S_AXI_AWSIZE_I),
	.S_AXI_AWADDR_I(S_AXI_AWADDR_I),
	.SENSOR_DONE_I(SENSOR_DONE_I),
	.M_OCM_WSTRB_O(M_OCM_WSTRB_O),
	.M_OCM_BREADY_O(M_OCM_BREADY_O),
	.SENSOR_DATA_I(SENSOR_DATA),
	.ACTOR_DATA_O(ACTOR_DATA)
);

endmodule
