`include "ultrasynth.vh"
`include "cgra.vh"
module SyncUnit #
(
	parameter integer CONTEXT_ADDR_WIDTH = -1,
	parameter integer CYCLE_COUNTER_WIDTH = -1,
	parameter integer INCOMING_DATA_WIDTH = -1,
	parameter integer RUN_COUNTER_WIDTH = -1
)
(
	input wire EN_I,
	input wire CGRA_CLK_I,
	input wire RST_N_I,
	input wire STATE_CHANGE_I,
	input wire INTERVAL_CHANGE_I,
	input wire [INCOMING_DATA_WIDTH-1:0] DATA_I,
	input wire SENSOR_WRITES_COMPLETE_I,
	input wire RUN_STARTED_I,
	output wire TRIGGER_RUN_O,
	output wire IS_HYBRID_O,
	output wire SYNC_IN_O,
	output wire [CONTEXT_ADDR_WIDTH-1:0] START_ADDR_O,
	output wire [RUN_COUNTER_WIDTH-1:0] RUN_COUNTER
);

// declarations
reg run; // meta state of an execution cycle, tells if a CGRA EXECUTE may be triggered
reg hybrid; // specifies if CGRA and Zynq-PS are working together
reg trigger_run;
reg sync_in;
reg [CONTEXT_ADDR_WIDTH-1:0] start_word; // the ccnt value to start from
reg [CYCLE_COUNTER_WIDTH-1:0] cycle_counter; // forces sync_in at value 0, if run is set
reg [CYCLE_COUNTER_WIDTH-1:0] interval_length; // upper boundary for cycle counter
reg [RUN_COUNTER_WIDTH-1:0] run_counter;

// output assignments
assign START_ADDR_O = start_word;
assign TRIGGER_RUN_O = trigger_run;
assign IS_HYBRID_O = hybrid;
assign SYNC_IN_O = sync_in;
assign RUN_COUNTER = run_counter;

// handle start address and state changes
always @(posedge CGRA_CLK_I) begin
	if (RST_N_I == 1'b0) begin
		run <= 1'b0;
		hybrid <= 1'b0;
		start_word <= 0;
		interval_length <= {CYCLE_COUNTER_WIDTH{1'b1}};
	end
	else if (EN_I) begin
		if (STATE_CHANGE_I) begin
			hybrid <= DATA_I[CONTEXT_ADDR_WIDTH+1];
			run <= DATA_I[CONTEXT_ADDR_WIDTH];
			if (~run)
				start_word <= DATA_I[CONTEXT_ADDR_WIDTH-1:0];
		end
		if (INTERVAL_CHANGE_I) 
			interval_length <= DATA_I[CYCLE_COUNTER_WIDTH-1:0];
	end
end

// run management
always @(posedge CGRA_CLK_I) begin
	if (~RST_N_I) begin
		sync_in <= 1'b0;
		trigger_run <= 1'b0;
		run_counter <= 0;
		cycle_counter <= 0;
	end else if (EN_I) begin
		if (cycle_counter == interval_length) begin
			run_counter <= run_counter + 1;
			cycle_counter <= 0;
		end else if (run)
			cycle_counter <= cycle_counter + 1;

		if (run && cycle_counter == 0)
			sync_in <= 1'b1;
		else
			sync_in <= 1'b0;

		if (RUN_STARTED_I)
			trigger_run <= 1'b0;
		else if (run && SENSOR_WRITES_COMPLETE_I)
			trigger_run <= 1'b1;
	end
end

endmodule