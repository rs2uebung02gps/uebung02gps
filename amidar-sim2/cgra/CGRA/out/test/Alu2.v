/*
Author: Dennis L. Wolf
Date: Mon May 29 13:28:06 CEST 2017
Version: 1.3 (Changed to String Template Interface and ultrasynth)
Version History: 1.2 Code Review and Cleanup
		 1.1 debugged and simulated
		 1.0 layout & concept
*/
// `timescale 1 ns / 1 ps

`include "cgra.vh" // import definitions of parameters and types

`default_nettype wire

module Alu2 #( parameter OPCODE_WIDTH = 0)(
    
  input wire signed [32-1:0] OPERAND_A,
  input wire signed [32-1:0] OPERAND_B,output reg signed  [32-1:0] RESULT_O , 
  input wire [OPCODE_WIDTH-1:0] OPCODE_I);

// Listing of the opcodes for the available operations
parameter NOP = 0;
parameter IADD = 1;
parameter ISUB = 2;
parameter IOR = 3;
parameter IAND = 4;
parameter IXOR = 5;
parameter ISHL = 6;
parameter ISHR = 7;
parameter IUSHR = 8;


//Instanciation of the operator modules - for each available operation there is a distinct module.

// Module for the operation NOP
wire [32-1:0] result_NOP;

NOP module_NOP (
.OP_A_I(OPERAND_A[32-1: 0]),  .RESULT_O(result_NOP)
);
// Module for the operation IADD
wire [32-1:0] result_IADD;

IADD module_IADD (
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_IADD)
);
// Module for the operation ISUB
wire [32-1:0] result_ISUB;

ISUB module_ISUB (
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_ISUB)
);
// Module for the operation IOR
wire [32-1:0] result_IOR;

IOR module_IOR (
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_IOR)
);
// Module for the operation IAND
wire [32-1:0] result_IAND;

IAND module_IAND (
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_IAND)
);
// Module for the operation IXOR
wire [32-1:0] result_IXOR;

IXOR module_IXOR (
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_IXOR)
);
// Module for the operation ISHL
wire [32-1:0] result_ISHL;

ISHL module_ISHL (
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_ISHL)
);
// Module for the operation ISHR
wire [32-1:0] result_ISHR;

ISHR module_ISHR (
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_ISHR)
);
// Module for the operation IUSHR
wire [32-1:0] result_IUSHR;

IUSHR module_IUSHR (
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_IUSHR)
);


/* This block contains the result connection. This is basically a big multiplexer that switches
   the wires Result, Cache Valid, Cache Write and Status. Depending on whether the PE contains 
   control flow or memory accesing operations the designated wiring appears */
always@(*) begin
  case (OPCODE_I)
   NOP: begin 
      
      RESULT_O = result_NOP;
   end

   IADD: begin 
      
      RESULT_O = result_IADD;
   end

   ISUB: begin 
      
      RESULT_O = result_ISUB;
   end

   IOR: begin 
      
      RESULT_O = result_IOR;
   end

   IAND: begin 
      
      RESULT_O = result_IAND;
   end

   IXOR: begin 
      
      RESULT_O = result_IXOR;
   end

   ISHL: begin 
      
      RESULT_O = result_ISHL;
   end

   ISHR: begin 
      
      RESULT_O = result_ISHR;
   end

   IUSHR: begin 
      
      RESULT_O = result_IUSHR;
   end


   default: begin
	RESULT_O = OPERAND_B;
	end
  endcase
end
endmodule

