/*
Author: Dennis L. Wolf
Date: Mon May 29 13:28:06 CEST 2017
Version: 2.0 (changed to String templates)
Version History: 1.1 debugged and simulated
		 1.0 layout & concept
*/

`default_nettype wire


module CBox (
input wire 			CLK_I,
input wire			RST_I,   // TODO - test if this is really neccesssary ...
input wire			EN_I,
input wire 			STATUS_3_I, 

input wire CONTEXT_EN_I,
input wire CONTEXT_WR_EN_I,
input wire [8-1:0]	CONTEXT_WR_ADDR_I,
input wire [40-1:0]	CONTEXT_DATA_I,
input wire [8-1:0]	CCNT_I,
output reg w_reg_in_positive,
output reg w_reg_in_negative,
input wire w_reg_predication,
input wire w_reg_or_positive,
input wire w_reg_or_negative,
output wire[8-4:0] w_rd_addr_or_positive,
output wire[13-9:0] w_rd_addr_or_negative,
output wire[18-14:0] w_rd_addr_predication,
output wire[23-19:0] w_rd_addr_predication1,
output wire[28-24:0] w_rd_addr_predication2,

output wire[33-29:0] w_wr_addr_negative,
output wire[38-34:0] w_wr_addr_positive,

output wire w_wr_en

);


// Context memory to configure the CBox
 (* ram_style = "block" *) reg [40-1:0] contextmemory [256-1:0];
reg [40-1:0] contextmemoryout;


// Mask to decode the context memory

// driver to bypass the and gate on side B
wire w_bypass_and_negative;
assign w_bypass_and_negative = contextmemoryout[0];

// driver to bypass the and gate on side A 
wire w_bypass_and_positive;
assign w_bypass_and_positive = contextmemoryout[1];

// driver to bypass the or gate on side B
wire w_bypass_or_negative;
assign w_bypass_or_negative = contextmemoryout[2];

// driver to bypass the or gate on side A
wire w_bypass_or_positive;
assign w_bypass_or_positive = contextmemoryout[3];

// Read address for read port b1
assign w_rd_addr_or_positive = contextmemoryout[8:4];

// Read address for read port b2
assign w_rd_addr_or_negative = contextmemoryout[13:9];

// Read address for read port a
assign w_rd_addr_predication = contextmemoryout[18:14];
assign w_rd_addr_predication1 = contextmemoryout[23:19];assign w_rd_addr_predication2 = contextmemoryout[28:24];


// Write address for side B
assign w_wr_addr_negative = contextmemoryout[33:29];

// Write address for side A
assign w_wr_addr_positive = contextmemoryout[38:34];

// Driver to select the status in
// Write enable for the condition memory

assign w_wr_en = contextmemoryout[39];


// contextmanagment
always@(posedge CLK_I) begin
  if(/*EN_I && */CONTEXT_WR_EN_I) begin
     contextmemory[CONTEXT_WR_ADDR_I] <= CONTEXT_DATA_I;
  end
if(CONTEXT_EN_I) begin
  contextmemoryout <= contextmemory[CCNT_I];
  end
end


assign w_status = STATUS_3_I; 



// Conjunction logic for side A in front of the condition memory
always@* begin
	if(w_bypass_or_positive && w_bypass_and_positive)
		w_reg_in_positive = w_status;
	else if(w_bypass_or_positive && !w_bypass_and_positive)
		w_reg_in_positive = w_status && w_reg_predication;
	else if(! w_bypass_or_positive && w_bypass_and_positive)
		w_reg_in_positive = w_status || w_reg_or_positive;
	else 
		w_reg_in_positive = w_status && w_reg_predication || w_reg_or_positive;

end
// Conjunction logic for side B in front of the condition memory
always@* begin
	if(w_bypass_or_negative && w_bypass_and_negative)
		w_reg_in_negative = !w_status;
	else if(w_bypass_or_negative && !w_bypass_and_negative)
		w_reg_in_negative = !w_status && w_reg_predication;
	else if(! w_bypass_or_negative && w_bypass_and_negative)
		w_reg_in_negative = !w_status || w_reg_or_negative;
	else 
		w_reg_in_negative = !w_status && w_reg_predication || w_reg_or_negative;

end


endmodule
  
