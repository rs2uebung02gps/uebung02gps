/*
Author: Lukas J. Jung
Date: Mon May 29 13:28:06 CEST 2017
Version: 2.0 (changed to String templates)
Version History: 1.0 debugged and simulated
		 1.0 layout & concept
*/

`default_nettype wire


module CBoxWrapper (
input wire 			CLK_I,
input wire			RST_I,   // TODO - test if this is really neccesssary ...
input wire			EN_I,
input wire 			STATUS_3_I, 

input wire CONTEXT_EN_I,
input wire [2-1:0]CONTEXT_WR_EN_I,
input wire [8-1:0]	CONTEXT_WR_ADDR_I,
input wire [40-1:0]	CONTEXT_DATA_I,
input wire [8-1:0]	CCNT_I,
output reg [6-1:0]			PREDICATION_O,
output reg			BRANCH_SELECTION_O);

integer i;

// Condition memory - stores intermediate and conjuncted status'
reg [32-1:0] memory; // TODO - BRAM?


wire w_wr_en  [2-1:0];
wire [5-1:0] w_wr_addr_positive[2-1:0];
wire [5-1:0] w_wr_addr_negative[2-1:0];
wire [5-1:0] w_rd_addr_or_positive[2-1:0];
wire [5-1:0] w_rd_addr_or_negative[2-1:0];
wire [5-1:0] w_rd_addr_predication[2-1:0];

wire [5-1:0] w_rd_addr_predication1[2-1:0];wire [5-1:0] w_rd_addr_predication2[2-1:0];

wire w_reg_in_positive [2-1:0];
wire w_reg_in_negative [2-1:0];

reg w_reg_or_positive[2-1:0];
reg w_reg_or_negative[2-1:0];
reg w_reg_predication[2-1:0];




genvar n;
generate
    for (n=0; n<2; n=n+1) begin : generatedCBox
    CBox cBox(
	.CLK_I(CLK_I),
	.RST_I(RST_I),
	.EN_I(EN_I),
	.CCNT_I(CCNT_I), 
  .STATUS_3_I(STATUS_3_I),
        .CONTEXT_EN_I(CONTEXT_EN_I),
	.CONTEXT_DATA_I(CONTEXT_DATA_I),
	.CONTEXT_WR_ADDR_I(CONTEXT_WR_ADDR_I),
	.CONTEXT_WR_EN_I(CONTEXT_WR_EN_I[n]),
	.w_reg_in_positive(w_reg_in_positive[n]),
	.w_reg_in_negative(w_reg_in_negative[n]),
	.w_reg_predication(w_reg_predication[n]),
	.w_reg_or_positive(w_reg_or_positive[n]),
	.w_reg_or_negative(w_reg_or_negative[n]),
	.w_rd_addr_or_positive(w_rd_addr_or_positive[n]),
	.w_rd_addr_or_negative(w_rd_addr_or_negative[n]),
	.w_rd_addr_predication(w_rd_addr_predication[n]),
	.w_rd_addr_predication1(w_rd_addr_predication1[n]),
	.w_rd_addr_predication2(w_rd_addr_predication2[n]),

	.w_wr_addr_negative(w_wr_addr_negative[n]),
	.w_wr_addr_positive(w_wr_addr_positive[n]),
	.w_wr_en(w_wr_en[n])       
        );
end 
endgenerate





// condition memory managment
always@(posedge CLK_I) begin
 if(RST_I) begin	// RESET 
   for (i = 0; i < 32; i=i+1) begin
     memory[i] <= 0;
    end
  end // END RESET
      else begin 
	for(i = 0; i < 2; i=i+1) begin	
		if(w_wr_en[i] && EN_I) begin // BEHAVIOR
    			memory[w_wr_addr_positive[i]] <= w_reg_in_positive[i];
    			memory[w_wr_addr_negative[i]] <= w_reg_in_negative[i];
  		end
	end
  end // END BEHAVIOR
end // close always

// TODO - redesign for arbitrary swtiching
always@(*) begin
  for(i = 0; i < 2; i=i+1) begin	
    w_reg_predication[i] = memory[w_rd_addr_predication[i]];
    PREDICATION_O[0+i*6/2] = w_reg_predication[i];
    PREDICATION_O[1+i*3] = memory[w_rd_addr_predication1[i]];PREDICATION_O[2+i*3] = memory[w_rd_addr_predication2[i]];
    w_reg_or_positive[i] = memory[w_rd_addr_or_positive[i]];
    w_reg_or_negative[i] = memory[w_rd_addr_or_negative[i]]; 
  end
  BRANCH_SELECTION_O = w_reg_in_negative[0];

end

endmodule
