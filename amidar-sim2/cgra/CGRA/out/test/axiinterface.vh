`ifndef AXI_DEFS
`define AXI_DEFS

  `define   SYSTEM_ADDR_WIDTH 32

  `define   OP_WIDTH 2
  `define   WRITE_CONTEXT_PE 0
  `define   WRITE_CONTEXT_OTHER 1
  `define   WRITE_PARAMETER 2  
  `define   HOST_STATE_CHANGE 3

  `define   ID_PE0 0
  `define   ID_PELOG0 128
  `define   ID_PE1 1
  `define   ID_PELOG1 129
  `define   ID_PE2 2
  `define   ID_PELOG2 130
  `define   ID_PE3 3
  `define   ID_PELOG3 131

  `define   ID_CCU 0
  `define   ID_CBOX 1
  `define   ID_IDC 2
  `define   ID_GLOG 3
  `define   ID_LOG_DEST 4
  `define   ID_LOG_DEST_BOUND 5 
  `define   ID_LOG_DEST_INC 6
  `define   ID_OCM_DEST 7
  `define   ID_OCM_DEST_BOUND 8
  `define   ID_OCM_DEST_INC 9
  `define   ID_OCM_CONTEXT 10
  `define   ID_ACTOR 13
  `define   ID_SENSOR 14
  `define   ID_INTERVAL 11
  `define   ID_CONST_BUF 12

  `define   MASTER_DATA_WIDTH 64
  `define   SLAVE_DATA_WIDTH 32

`endif // AXI_DEFS

