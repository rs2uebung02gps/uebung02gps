/*
Author: Dennis L. Wolf
Date: 
Version: 2.0 (changed to String Template Interface - reduced to one template)
Version History: 1.2 Code Review and Cleanup
		 1.1 debugged and simulated
		 1.0 layout & concept

Comments: Combinatorial read. Sequential write. 2 read port - one direct out, one to load variable to the PEs ALU. If 
					the PE has memory accessing operation, there is a third read port to load data for memory stores.

entries: 32
*/


`include "cgra.vh" 

`default_nettype wire

module Registerfile_0(
  input  wire   			CLK_I,
  input  wire   			EN_I, 
  input  wire [6-1:0]	RD_PORT_DIRECT_ADDR_I,output wire [32-1:0] 	RD_PORT_DIRECT_O,
  input  wire [6-1:0]	RD_PORT_MUX_ADDR_I,output wire [32-1:0] 	RD_PORT_MUX_O, 
  input  wire   			WR_PORT_EN_I,
  input  wire [5-1:0]  WR_PORT_ADDR_I,input  wire [32-1:0] 	WR_PORT_DATA_I);


// Registerfile
reg signed [32-1:0] memory [32-1:0];

// sequential write
always@(posedge CLK_I) begin
if (WR_PORT_EN_I && EN_I) begin
  memory[WR_PORT_ADDR_I] <=  WR_PORT_DATA_I; 
 end
end 

//combinatorial reads
wire [32-1:0] const_direct_o_w, const_mux_o_w;
assign const_direct_o_w ={ { 32-6+1{RD_PORT_DIRECT_ADDR_I[6-2]}},RD_PORT_DIRECT_ADDR_I[6-2:0]};
assign const_mux_o_w    ={ { 32-6+1{RD_PORT_MUX_ADDR_I[6-2]}},RD_PORT_MUX_ADDR_I[6-2:0]};
assign RD_PORT_DIRECT_O =RD_PORT_DIRECT_ADDR_I[6-1]?const_direct_o_w : memory[RD_PORT_DIRECT_ADDR_I[6-2:0]];
assign RD_PORT_MUX_O    =RD_PORT_MUX_ADDR_I[6-1]?const_mux_o_w : memory[RD_PORT_MUX_ADDR_I[6-2:0]]; endmodule
