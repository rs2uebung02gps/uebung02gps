/*
Author: Dennis L. Wolf
Date: Mon May 29 13:28:06 CEST 2017
Version: 3.0 - generator target dependent
Version History: 	2.1 converters for contexts, Fsm reduction and cleanup
			2.0 changed to String Templates
			1.1 changed to header and `define
			1.0 construct
*/

`include "axiinterface.vh"
`include "ultrasynth.vh"
`include "sensorif.vh"
`include "actorif.vh"
`include "constbuf.vh"
`include "cgra.vh"

module Cgra_Ultrasynth #(
  parameter integer CONTEXT_ADDR_WIDTH = 8,
  parameter integer MAX_CONTEXT_ADDR_WIDTH = 8,
  parameter integer GLOG_CONTEXT_ADDR_WIDTH = 8,
  parameter integer RF_ADDR_WIDTH = 6, // the max RF address width (out of all PEs)
  parameter integer OTHERID_WIDTH = 4,
  parameter integer PEID_WIDTH = 2,
  parameter integer CONTEXT_SIZE = 256,
  parameter integer C_S_AXI_DATA_WIDTH  = 32,
  parameter integer C_S_AXI_ADDR_WIDTH  = 32
) 
(
(* dont_touch = "true" *) input wire S_AXI_RREADY_I,
(* dont_touch = "true" *) input wire [3-1:0] S_AXI_AWSIZE_I,
(* dont_touch = "true" *) input wire [2-1:0] S_AXI_AWBURST_I,
(* dont_touch = "true" *) output wire [2-1:0] M_OCM_AWBURST_O,
(* dont_touch = "true" *) output wire [2-1:0] S_AXI_RRESP_O,
(* dont_touch = "true" *) output wire S_AXI_RLAST_O,
(* dont_touch = "true" *) output wire S_AXI_BVALID_O,
(* dont_touch = "true" *) output wire M_LOG_BREADY_O,
(* dont_touch = "true" *) output wire [64-1:0] M_OCM_WDATA_O,
(* dont_touch = "true" *) output wire M_LOG_WLAST_O,
(* dont_touch = "true" *) output wire [3-1:0] M_OCM_AWSIZE_O,
(* dont_touch = "true" *) input wire S_AXI_ACLK_I,
(* dont_touch = "true" *) input wire [8-1:0] S_AXI_ARLEN_I,
(* dont_touch = "true" *) input wire [32-1:0] S_AXI_WDATA_I,
(* dont_touch = "true" *) output wire SENSOR_SYNC_IN_O,
(* dont_touch = "true" *) output wire S_AXI_AWREADY_O,
(* dont_touch = "true" *) input wire [2-1:0] S_AXI_ARBURST_I,
(* dont_touch = "true" *) input wire [3-1:0] S_AXI_ARSIZE_I,
(* dont_touch = "true" *) output wire M_OCM_AWVALID_O,
(* dont_touch = "true" *) input wire M_LOG_BVALID_I,
(* dont_touch = "true" *) output wire M_OCM_BREADY_O,
(* dont_touch = "true" *) output wire M_OCM_WLAST_O,
(* dont_touch = "true" *) input wire [32-1:0] S_AXI_ARADDR_I,
(* dont_touch = "true" *) input wire M_OCM_BVALID_I,
(* dont_touch = "true" *) output wire [32-1:0] ACTOR_DATA_O,
(* dont_touch = "true" *) output wire [8-1:0] M_LOG_AWLEN_O,
(* dont_touch = "true" *) input wire SENSOR_DONE_I,
(* dont_touch = "true" *) input wire S_AXI_AWVALID_I,
(* dont_touch = "true" *) output wire [64-1:0] M_LOG_WDATA_O,
(* dont_touch = "true" *) input wire M_OCM_WREADY_I,
(* dont_touch = "true" *) output wire [32-1:0] M_LOG_AWADDR_O,
(* dont_touch = "true" *) output wire [32-1:0] S_AXI_RDATA_O,
(* dont_touch = "true" *) input wire [32-1:0] S_AXI_AWADDR_I,
(* dont_touch = "true" *) input wire M_LOG_WREADY_I,
(* dont_touch = "true" *) output wire M_LOG_AWVALID_O,
(* dont_touch = "true" *) output wire ERROR_O,
(* dont_touch = "true" *) input wire S_AXI_BREADY_I,
(* dont_touch = "true" *) input wire [2-1:0] M_LOG_BRESP_I,
(* dont_touch = "true" *) output wire [8-1:0] M_LOG_WSTRB_O,
(* dont_touch = "true" *) output wire S_AXI_RVALID_O,
(* dont_touch = "true" *) output wire [2-1:0] S_AXI_BRESP_O,
(* dont_touch = "true" *) input wire [32-1:0] SENSOR_DATA_I,
(* dont_touch = "true" *) output wire M_LOG_WVALID_O,
(* dont_touch = "true" *) input wire M_LOG_AWREADY_I,
(* dont_touch = "true" *) input wire S_AXI_ARVALID_I,
(* dont_touch = "true" *) input wire [2-1:0] M_OCM_BRESP_I,
(* dont_touch = "true" *) output wire [32-1:0] M_OCM_AWADDR_O,
(* dont_touch = "true" *) output wire S_AXI_WREADY_O,
(* dont_touch = "true" *) output wire [8-1:0] M_OCM_WSTRB_O,
(* dont_touch = "true" *) output wire [2-1:0] M_LOG_AWBURST_O,
(* dont_touch = "true" *) output wire M_OCM_WVALID_O,
(* dont_touch = "true" *) input wire S_AXI_WLAST_I,
(* dont_touch = "true" *) output wire [3-1:0] M_LOG_AWSIZE_O,
(* dont_touch = "true" *) output wire [8-1:0] M_OCM_AWLEN_O,
(* dont_touch = "true" *) input wire M_OCM_AWREADY_I,
(* dont_touch = "true" *) output wire S_AXI_ARREADY_O,
(* dont_touch = "true" *) input wire S_AXI_WVALID_I,
(* dont_touch = "true" *) input wire [4-1:0] S_AXI_WSTRB_I,
(* dont_touch = "true" *) output wire ACTOR_SYNC_OUT_O,
(* dont_touch = "true" *) input wire [8-1:0] S_AXI_AWLEN_I
);
// --- assignments to map to internal wire names
wire CGRA_CLK;
assign CGRA_CLK = CLK_I;
wire RST_N_I;
assign RST_N_I = ~RST_I;

// --- forward declarations
wire [8-1:0] w_ccnt;

wire [32-1:0] w_direct_out_0;
wire [32-1:0] w_direct_out_1;
wire [32-1:0] w_direct_out_2;
wire [32-1:0] w_direct_out_3;

wire [32-1:0] w_pe_out_0;
wire [32-1:0] w_pe_out_1;
wire [32-1:0] w_pe_out_2;

// sensor
wire sync_in;
wire [`DATA_WIDTH-1:0] sensor_data;
wire [`SENSOR_ID_WIDTH-1:0] sensor_addr;
wire sensor_read_enable;
assign SENSOR_SYNC_IN_O = sync_in;

// actor
reg [`DATA_WIDTH-1:0] actor_data;
wire [`ACTOR_ID_WIDTH-1:0] actor_addr;
wire [`PE_ID_WIDTH-1:0] actor_pe_id;
wire actor_write_enable;
wire sync_out;
assign ACTOR_SYNC_OUT_O = sync_out;
assign ACTOR_DATA_O = actor_data;

// SyncUnit
wire [CONTEXT_ADDR_WIDTH-1:0] start_addr;
wire hybrid;
wire start_exec;
wire [16-1:0] run_counter;

// AXI interface
wire enable_write_ctrl;
wire valid_context;
wire [`OP_WIDTH-1:0] operation;
wire [8-1:0] affected_module;
wire [PEID_WIDTH+RF_ADDR_WIDTH-1:0] parameter_buffer_destination;
wire [MAX_CONTEXT_ADDR_WIDTH-1:0] context_addr;

wire ctrl_contextEn_pe0;
wire ctrl_contextEn_pe1;
wire ctrl_contextEn_pe2;
wire ctrl_contextEn_pe3;

wire is_peLog_context;
wire ccu_context_wr_en;
wire cbox_context_wr_en;
wire idc_wr_en;
wire parameter_buffer_wr_en;
wire syncUnit_state_change;
wire syncUnit_interval_change;
wire glog_context_wr_en;
wire sensor_context_wr_en;
wire actor_context_wr_en;
wire context_wr_en;
wire logDestWrEn;
wire logDestBoundWrEn;
wire logDestIncWrEn;
wire ocmDestWrEn;
wire ocmDestBoundWrEn;
wire ocmDestIncWrEn;
wire ocmContextWrEn;
wire constBufferWrEn;

wire [C_S_AXI_DATA_WIDTH-1:0] w_incoming_context_data_0; // + w_incoming_context_data_x, ... +1 for each 32 bit of max(maxContextWidth, contextWidthCBox)
wire [`DATA_WIDTH-1:0] w_incoming_context_data_1;

wire [64-1:0] context_data;
assign context_data = {w_incoming_context_data_0, w_incoming_context_data_1};

// parameter buffer
wire parameter_buffer_empty;
wire parameter_buffer_full;
wire [`DATA_WIDTH-1:0] parameter_data;
wire [PEID_WIDTH-1:0] parameter_target_pe;
wire [RF_ADDR_WIDTH-1:0] rf_wr_addr;

wire ctrl_parameterEn_pe0;
wire ctrl_parameterEn_pe1;
wire ctrl_parameterEn_pe2;
wire ctrl_parameterEn_pe3;

wire parameter_context_preped;

// ConstBuf
wire [32-1:0] const_buf_data_pe0;
wire [32-1:0] const_buf_data_pe1;
wire [32-1:0] const_buf_data_pe2;
wire [32-1:0] const_buf_data_pe3;


// additional Ultrasynth PE outs
wire [1-1:0] const_buf_read_enable_pe0;
wire [1-1:0] const_buf_read_enable_pe1;
wire [1-1:0] const_buf_read_enable_pe2;
wire [1-1:0] const_buf_read_enable_pe3;
wire [5-1:0] const_buf_addr_pe0;
wire [5-1:0] const_buf_addr_pe1;
wire [5-1:0] const_buf_addr_pe2;
wire [5-1:0] const_buf_addr_pe3;


// --- FSM wires, registers and state definition

reg [`STATE_WIDTH-1:0] state, next_state;

// wires to output regs
reg ccu_load_en;
reg enable_datapath_modules; // PE and CBOX enable
reg parameter_update_allowed;
reg error;
reg [`ERROR_WIDTH-1:0] err_vec;

// actual outputs
reg ccu_load_en_sync;
reg enable_datapath_modules_sync;
reg parameter_update_allowed_sync;
reg error_sync;
reg [`ERROR_WIDTH-1:0] err_vec_sync;

// helpful stuff
reg parameter_update_oneMoreCycle;
wire parameter_update_underway;
assign parameter_update_underway = parameter_context_preped || parameter_update_oneMoreCycle;
assign ERROR_O = error_sync;

// --- FSM BEGIN
always @(posedge CGRA_CLK)
begin
  if (~RST_N_I)
    begin
      state <= `IDLE;
      parameter_update_oneMoreCycle <= 1'b0;
      ccu_load_en_sync <= 1'b0;
      enable_datapath_modules_sync <= 1'b0;
      parameter_update_allowed_sync <= 1'b0;
      error_sync <= 1'b1;
      err_vec_sync <= 0;
    end
  else if (EN_I)
    begin
    	// management
      state <= next_state;
      parameter_update_oneMoreCycle <= parameter_context_preped;

      // outputs
      ccu_load_en_sync <= ccu_load_en;
      enable_datapath_modules_sync <= enable_datapath_modules;
      parameter_update_allowed_sync <= parameter_update_allowed;
      error_sync <= error;
      err_vec_sync <= err_vec;
    end
end

// state switching
always @(*)
begin
  case(state)
  `UPDATE_PARAMETER: begin
    if (start_exec && ~hybrid)
      next_state = `START;
    else if (parameter_update_underway)
      next_state = `UPDATE_PARAMETER;
    else 
      next_state = `IDLE;
  end
  `EXECUTE: begin
  	if (sync_in)
  		next_state = `ERROR;
    else if (w_ccnt == 256-1)
      next_state = `IDLE;
    else
      next_state = `EXECUTE;
  end
  `START: begin
    next_state = `EXECUTE;
  end
  `IDLE: begin
    if ( start_exec && !(~parameter_buffer_empty && hybrid) )
      next_state = `START;
    else if (parameter_update_underway)
      next_state = `UPDATE_PARAMETER;
    else
      next_state = `IDLE;
  end
  `ERROR: begin
  	next_state = `ERROR;
  end
  default: next_state = `ERROR;
  endcase
end

// output generation
always @(*)
begin
  // `EXECUTE state
  ccu_load_en = 1'b0;
  enable_datapath_modules = 1'b1;
  parameter_update_allowed = 1'b0;
  error = 1'b0;
  err_vec = 0;

  if (next_state == `START)
    ccu_load_en = 1'b1;
  else if (next_state == `UPDATE_PARAMETER)
  	parameter_update_allowed = ~parameter_buffer_empty && (~start_exec || hybrid);
  else if (next_state == `IDLE) begin
    enable_datapath_modules = 1'b0;
    parameter_update_allowed = ~parameter_buffer_empty && (~start_exec || hybrid);
  end else if (next_state == `ERROR) begin
  	enable_datapath_modules = 1'b0;
    parameter_update_allowed = 1'b0;
    error = 1'b1;
    err_vec[`ERROR_WIDTH-1] = sync_in && state == `EXECUTE || err_vec_sync[`ERROR_WIDTH-1];
  end
end
// --- FSM END

ComUnit #
(
  .CONTEXT_ADDR_WIDTH(MAX_CONTEXT_ADDR_WIDTH),
  .RF_ADDR_WIDTH(RF_ADDR_WIDTH),
  .OTHERID_WIDTH(OTHERID_WIDTH),
  .PEID_WIDTH(PEID_WIDTH),
  .C_S_AXI_DATA_WIDTH(C_S_AXI_DATA_WIDTH),
  .C_S_AXI_ADDR_WIDTH(C_S_AXI_ADDR_WIDTH)
)
axi_interface
(
  .S_AXI_ACLK_I(S_AXI_ACLK_I),
  .CGRA_CLK(CGRA_CLK),
  .RST_N_I(RST_N_I),
  .EN_I(EN_I),
  .S_AXI_AWADDR_I(S_AXI_AWADDR_I),
  .S_AXI_AWLEN_I(S_AXI_AWLEN_I),
  .S_AXI_AWSIZE_I(S_AXI_AWSIZE_I),
  .S_AXI_AWBURST_I(S_AXI_AWBURST_I),
  .S_AXI_AWVALID_I(S_AXI_AWVALID_I),
  .S_AXI_AWREADY_O(S_AXI_AWREADY_O),
  .S_AXI_WDATA_I(S_AXI_WDATA_I),
  .S_AXI_WSTRB_I(S_AXI_WSTRB_I),
  .S_AXI_WLAST_I(S_AXI_WLAST_I),
  .S_AXI_WVALID_I(S_AXI_WVALID_I),
  .S_AXI_WREADY_O(S_AXI_WREADY_O),
  .S_AXI_BRESP_O(S_AXI_BRESP_O),
  .S_AXI_BVALID_O(S_AXI_BVALID_O),
  .S_AXI_BREADY_I(S_AXI_BREADY_I),
  .S_AXI_ARADDR_I(S_AXI_ARADDR_I),
  .S_AXI_ARLEN_I(S_AXI_ARLEN_I),
  .S_AXI_ARSIZE_I(S_AXI_ARSIZE_I),
  .S_AXI_ARBURST_I(S_AXI_ARBURST_I),
  .S_AXI_ARVALID_I(S_AXI_ARVALID_I),
  .S_AXI_ARREADY_O(S_AXI_ARREADY_O),
  .S_AXI_RDATA_O(S_AXI_RDATA_O),
  .S_AXI_RRESP_O(S_AXI_RRESP_O),
  .S_AXI_RLAST_O(S_AXI_RLAST_O),
  .S_AXI_RVALID_O(S_AXI_RVALID_O),
  .S_AXI_RREADY_I(S_AXI_RREADY_I),

  .ENABLE_WRITE_CTRL_O(enable_write_ctrl),
  .VALID_CONTEXT_O(valid_context),
  .CONTEXT_ADDR_O(context_addr),
  .AFFECTED_MODULE_O(affected_module),
  .PARAMETER_BUFFER_DESTINATION_O(parameter_buffer_destination),
  .OPERATION_O(operation),
  .DATA_0_O(w_incoming_context_data_0),
  .DATA_1_O(w_incoming_context_data_1),

  .WR_EN_IDC_I(idc_wr_en),
  .PARAMETER_BUFFER_FULL_I(parameter_buffer_full),
  .PARAMETER_CLEANUP_I(parameter_update_allowed_sync)
);

WriteControl #
(
  .MAX_ID_WIDTH(8),
  .OTHER_ID_WIDTH(OTHERID_WIDTH),
  .PE_ID_WIDTH(PEID_WIDTH)
)
wr_cntrl
( 
  .CGRA_CLK(CGRA_CLK),
  .EN_I(EN_I),
  .RST_N_I(RST_N_I),
  .EN_CONTROLLER_I(enable_write_ctrl),
  .VALID_CONTEXT_I(valid_context),
  .OPERATION_I(operation),
  .TRANSACTION_ID_I(affected_module),
  .PARAMETER_UPDATE_ALLOWED_I(parameter_update_allowed_sync),
  .PE_ID_I(parameter_target_pe),
  // control
  .IS_PELOG_CONTEXT_O(is_peLog_context),
	.CONTEXT_WREN_PE0_O(ctrl_contextEn_pe0),
	.PARAMETER_CONTEXT_WREN_PE0_O(ctrl_parameterEn_pe0),
	.CONTEXT_WREN_PE1_O(ctrl_contextEn_pe1),
	.PARAMETER_CONTEXT_WREN_PE1_O(ctrl_parameterEn_pe1),
	.CONTEXT_WREN_PE2_O(ctrl_contextEn_pe2),
	.PARAMETER_CONTEXT_WREN_PE2_O(ctrl_parameterEn_pe2),
	.CONTEXT_WREN_PE3_O(ctrl_contextEn_pe3),
	.PARAMETER_CONTEXT_WREN_PE3_O(ctrl_parameterEn_pe3),
	
	.CONTEXT_WREN_CCU_O(ccu_context_wr_en),
	.CONTEXT_WREN_CBOX_O(cbox_context_wr_en),
	.CONTEXT_WREN_IDC_O(idc_wr_en),
	.CONTEXT_WREN_GLOG_O(glog_context_wr_en),
	.CONTEXT_WREN_ACTOR_O(actor_context_wr_en),
	.CONTEXT_WREN_SENSOR_O(sensor_context_wr_en),
	.PARAMETER_BUFFER_WREN_O(parameter_buffer_wr_en),
	.SYNCUNIT_STATE_CHANGE_O(syncUnit_state_change),
	.SYNCUNIT_INTERVAL_CHANGE_O(syncUnit_interval_change),
	.LOG_DEST_CHANGE_O(logDestWrEn),
	.LOG_DEST_BOUND_CHANGE_O(logDestBoundWrEn),
	.LOG_DEST_INC_CHANGE_O(logDestIncWrEn),
	.OCM_DEST_CHANGE_O(ocmDestWrEn),
	.OCM_DEST_BOUND_CHANGE_O(ocmDestBoundWrEn),
	.OCM_DEST_INC_CHANGE_O(ocmDestIncWrEn),
	.CONTEXT_WREN_OCM_O(ocmContextWrEn),
	.CONST_BUF_WREN_O(constBufferWrEn),
	.PREPARE_PARAMETER_CONTEXT_O(parameter_context_preped),
	.PE_CONTEXT_ENABLE_O(context_wr_en)
);

ConstBuf constBuf (
	.CGRA_CLK_I(CGRA_CLK),
	.RST_N_I(RST_N_I),
	.EN_I(EN_I),
	.READ_ENABLE_PE0_I(const_buf_read_enable_pe0),
	.READ_ENABLE_PE1_I(const_buf_read_enable_pe1),
	.READ_ENABLE_PE2_I(const_buf_read_enable_pe2),
	.READ_ENABLE_PE3_I(const_buf_read_enable_pe3),
	.READ_ADDR_PE0_I(const_buf_addr_pe0),
	.READ_ADDR_PE1_I(const_buf_addr_pe1),
	.READ_ADDR_PE2_I(const_buf_addr_pe2),
	.READ_ADDR_PE3_I(const_buf_addr_pe3),
	.DATA_PE0_O(const_buf_data_pe0),
	.DATA_PE1_O(const_buf_data_pe1),
	.DATA_PE2_O(const_buf_data_pe2),
	.DATA_PE3_O(const_buf_data_pe3),

	.WRITE_ENABLE_I(constBufferWrEn),
	.WRITE_ADDR_I(context_addr[`CONST_BUF_ADDR_WIDTH-1:0]),
	.DATA_I(w_incoming_context_data_0)
);


ParameterBuffer #
(
	.BUFFER_SIZE(4),
	.PARAMETER_WIDTH(C_S_AXI_DATA_WIDTH),
	.PE_ID_WIDTH(PEID_WIDTH),
	.RF_WIDTH(RF_ADDR_WIDTH),
	.COUNTER_WIDTH(2)
)
parameterBuffer
(
	.EN_I(EN_I),
	.CGRA_CLK_I(CGRA_CLK),
	.RST_N_I(RST_N_I),
	.WRITE_EN_I(parameter_buffer_wr_en),
	.NEXT_I(parameter_update_allowed_sync),
	.DATA_I(w_incoming_context_data_0),
	.DESTINATION_I(parameter_buffer_destination),
	.FULL_O(parameter_buffer_full),
	.EMPTY_O(parameter_buffer_empty),
	.DATA_O(parameter_data),
	.DESTINATION_PE_O(parameter_target_pe),
	.DESTINATION_RF_OFFSET_O(rf_wr_addr)
);

SyncUnit #
(
	.CONTEXT_ADDR_WIDTH(CONTEXT_ADDR_WIDTH),
	.CYCLE_COUNTER_WIDTH(32),
	.INCOMING_DATA_WIDTH(32),
	.RUN_COUNTER_WIDTH(16)
)
syncUnit
(
	.EN_I(EN_I),
	.CGRA_CLK_I(CGRA_CLK),
	.RST_N_I(RST_N_I),
	.STATE_CHANGE_I(syncUnit_state_change),
	.INTERVAL_CHANGE_I(syncUnit_interval_change),
	.DATA_I(w_incoming_context_data_0[32-1:0]),
	.SENSOR_WRITES_COMPLETE_I(SENSOR_DONE_I),
	.RUN_STARTED_I(ccu_load_en_sync),
	.TRIGGER_RUN_O(start_exec),
	.IS_HYBRID_O(hybrid),
	.SYNC_IN_O(sync_in),
	.START_ADDR_O(start_addr),
	.RUN_COUNTER(run_counter)
);

Log log (
  .EN_I(EN_I),
  .CGRA_CLK_I(CGRA_CLK),
  .AXI_ACLK_I(S_AXI_ACLK_I),
  .RST_N_I(RST_N_I),
  .SYNC_IN_I(sync_in),
  .CCNT_I(w_ccnt),
  .CONTEXT_PE0_WREN_I(ctrl_contextEn_pe0),
  .CONTEXT_PE1_WREN_I(ctrl_contextEn_pe1),
  .CONTEXT_PE2_WREN_I(ctrl_contextEn_pe2),
  .CONTEXT_PE3_WREN_I(ctrl_contextEn_pe3),
 
  .GLOBAL_LOG_CONTEXT_WREN_I(glog_context_wr_en),
  .IS_LOG_CONTEXT_I(is_peLog_context),
  .DEST_WREN_I(logDestWrEn),
  .DEST_BOUND_WREN_I(logDestBoundWrEn),
  .DEST_INC_WREN_I(logDestIncWrEn),
  .OCM_DEST_WREN_I(ocmDestWrEn),
  .OCM_DEST_BOUND_WREN_I(ocmDestBoundWrEn),
  .OCM_DEST_INC_WREN_I(ocmDestIncWrEn),
  .OCM_CONTEXT_WREN_I(ocmContextWrEn),
  .CONTEXT_ADDR_I(context_addr[GLOG_CONTEXT_ADDR_WIDTH-1:0]),
  .DATA_I(w_incoming_context_data_0),
  .DIRECT_OUT_PE0_I(w_direct_out_0),
  .DIRECT_OUT_PE1_I(w_direct_out_1),
  .DIRECT_OUT_PE2_I(w_direct_out_2),
  .DIRECT_OUT_PE3_I(w_direct_out_3),
 
  .RUN_COUNTER_I(run_counter),
  .ERROR_I(err_vec_sync),
  .LOG_AWLEN_O(M_LOG_AWLEN_O),
  .LOG_AWSIZE_O(M_LOG_AWSIZE_O),
  .LOG_AWBURST_O(M_LOG_AWBURST_O),
  .LOG_AWADDR_O(M_LOG_AWADDR_O),
  .LOG_AWVALID_O(M_LOG_AWVALID_O),
  .LOG_AWREADY_I(M_LOG_AWREADY_I),
  .LOG_WDATA_O(M_LOG_WDATA_O),
  .LOG_WSTRB_O(M_LOG_WSTRB_O),
  .LOG_WLAST_O(M_LOG_WLAST_O),
  .LOG_WVALID_O(M_LOG_WVALID_O),
  .LOG_WREADY_I(M_LOG_WREADY_I),
  .LOG_BREADY_O(M_LOG_BREADY_O),
  .LOG_BVALID_I(M_LOG_BVALID_I),
  .LOG_BRESP_I(M_LOG_BRESP_I),
  .OCM_AWLEN_O(M_OCM_AWLEN_O),
  .OCM_AWSIZE_O(M_OCM_AWSIZE_O),
  .OCM_AWBURST_O(M_OCM_AWBURST_O),
  .OCM_AWADDR_O(M_OCM_AWADDR_O),
  .OCM_AWVALID_O(M_OCM_AWVALID_O),
  .OCM_AWREADY_I(M_OCM_AWREADY_I),
  .OCM_WDATA_O(M_OCM_WDATA_O),
  .OCM_WSTRB_O(M_OCM_WSTRB_O),
  .OCM_WLAST_O(M_OCM_WLAST_O),
  .OCM_WVALID_O(M_OCM_WVALID_O),
  .OCM_WREADY_I(M_OCM_WREADY_I),
  .OCM_BREADY_O(M_OCM_BREADY_O),
  .OCM_BVALID_I(M_OCM_BVALID_I),
  .OCM_BRESP_I(M_OCM_BRESP_I)
);

SensorIF #
(
	.CGRA_CONTEXT_ADDR_WIDTH(CONTEXT_ADDR_WIDTH),
	.CGRA_CONTEXT_SIZE(256)
)
sensor_interface
(
	.EN_I(EN_I),
	.CGRA_CLK_I(CGRA_CLK),
	.RST_N_I(RST_N_I),
	.CCNT_I(w_ccnt),
	.SENSOR_ADDR_O(sensor_addr),
	.SENSOR_READ_EN_O(sensor_read_enable),
	.CONTEXT_WRITE_EN_I(sensor_context_wr_en),
	.CONTEXT_ADDR_I(context_addr[CONTEXT_ADDR_WIDTH-1:0]),
	.CONTEXT_DATA_I(context_data[34:32])
);

ActorIF #
(
  .CGRA_CONTEXT_ADDR_WIDTH(CONTEXT_ADDR_WIDTH),
  .CGRA_CONTEXT_SIZE(256)
)
actor_interface
(
  .EN_I(EN_I),
  .CGRA_CLK_I(CGRA_CLK),
  .RST_N_I(RST_N_I),
  .CCNT_I(w_ccnt),
  .SYNC_OUT_O(sync_out),
  .ACTOR_WRITE_ADDR_O(actor_addr),
  .ACTOR_SOURCE_PE_ID_O(actor_pe_id),
  .ACTOR_WRITE_ENABLE_O(actor_write_enable),
  .CONTEXT_WRITE_EN_I(actor_context_wr_en),
  .CONTEXT_ADDR_I(context_addr[CONTEXT_ADDR_WIDTH-1:0]),
  .CONTEXT_DATA_I(context_data[37:32])
);


always @(*) begin
  case(actor_pe_id)
    0: actor_data = w_direct_out_0;
    1: actor_data = w_direct_out_1;
    2: actor_data = w_direct_out_2;
    3: actor_data = w_direct_out_3;

    default: actor_data = 0; 
  endcase
end

wire w_branch_selection;
wire [-1:0] w_predication;
wire ccu_enable;
assign ccu_enable = enable_datapath_modules_sync && EN_I;

ContextControlUnit #
(
  8,
  256
)
controlunit
(
	.CLK_I(CGRA_CLK),
	.RST_N_I(RST_N_I),
	.EN_I(ccu_enable),
	.CBOX_I(w_branch_selection),
  .CONTEXT_DATA_I(context_data[42:32]),
  .CONTEXT_WR_ADDR_I(context_addr[CONTEXT_ADDR_WIDTH-1:0]),
  .CONTEXT_WR_EN_I(ccu_context_wr_en),
  .ADDR_I(start_addr[8-1:0]),
  .LOAD_EN_I(ccu_load_en_sync),
  .CCNT_O(w_ccnt)
);

wire  w_status_3;
 



CBoxWrapper cBoxWrapper(
	.CLK_I(CLK_I),
	.RST_I(RST_I),
	.EN_I(w_enable_datapath_modules_sync),
	.CCNT_I(w_ccnt), 
	.STATUS_3_I(w_status_3),
  	.CONTEXT_EN_I(enable_datapath_modules_sync),
	.CONTEXT_DATA_I(context_data[39:0]),
	.CONTEXT_WR_ADDR_I(context_addr[CONTEXT_ADDR_WIDTH-1:0]),
	.CONTEXT_WR_EN_I(cbox_context_wr_en), //TODO: implement context write enable for additional cboxes
	.PREDICATION_O( w_predication),
	.BRANCH_SELECTION_O(w_branch_selection)            
        );


wire w_en_pe_context;
assign w_en_pe_context = (context_wr_en || enable_datapath_modules_sync || parameter_context_preped || start_exec) && EN_I;

wire [32-1:0] context_data_pe0;
wire [8-1:0] context_addr_pe0;
wire write_enable_context_pe0;
wire [32-1:0] preped_context_pe0;

assign context_data_pe0 = ctrl_parameterEn_pe0 ? preped_context_pe0 : parameter_update_oneMoreCycle ? 0 : context_data[31:0];
assign context_addr_pe0 = parameter_update_oneMoreCycle || ctrl_parameterEn_pe0 ? {8{1'b1}} : context_addr[CONTEXT_ADDR_WIDTH-1:0];
assign write_enable_context_pe0 = (ctrl_contextEn_pe0 && ~is_peLog_context) || ctrl_parameterEn_pe0 || parameter_update_oneMoreCycle;
assign preped_context_pe0 = { {1'b1},{1'b0},{1'b1},{12'b0},{rf_wr_addr[5-1:0]},{4'b0},{1'b1},{4'b0} };



   PE_0 pe_0( 
   .CLK_I(CLK_I),.EN_I(enable_datapath_modules_sync),
   .CCNT_I(w_ccnt), 
   .INPUT_1_I(w_direct_out_1),
   .INPUT_2_I(w_direct_out_2),
   .CONTEXT_EN_I(w_en_pe_context),
   .CONTEXT_DATA_I(context_data_pe0),
   .CONTEXT_WR_EN_I(write_enable_context_pe0),
   .CONTEXT_WR_ADDR_I(context_addr_pe0),.AMIDAR_I(parameter_data),
   .PREDICATION_I(w_predication),
   .DIRECT_O(w_direct_out_0),
   .LIVEOUT_O(w_pe_out_0) );


 //  wire [32-1:0] context_data_pe1;
wire [8-1:0] context_addr_pe1;
wire write_enable_context_pe1;
wire [32-1:0] preped_context_pe1;

assign context_data_pe1 = ctrl_parameterEn_pe1 ? preped_context_pe1 : parameter_update_oneMoreCycle ? 0 : context_data[31:0];
assign context_addr_pe1 = parameter_update_oneMoreCycle || ctrl_parameterEn_pe1 ? {8{1'b1}} : context_addr[CONTEXT_ADDR_WIDTH-1:0];
assign write_enable_context_pe1 = (ctrl_contextEn_pe1 && ~is_peLog_context) || ctrl_parameterEn_pe1 || parameter_update_oneMoreCycle;
assign preped_context_pe1 = { {1'b1},{1'b0},{1'b1},{12'b0},{rf_wr_addr[5-1:0]},{4'b0},{1'b1},{4'b0} };



   PE_1 pe_1( 
   .CLK_I(CLK_I),.EN_I(enable_datapath_modules_sync),
   .CCNT_I(w_ccnt), 
   .INPUT_0_I(w_direct_out_0),
   .INPUT_3_I(w_direct_out_3),
   .CONTEXT_EN_I(w_en_pe_context),
   .CONTEXT_DATA_I(context_data_pe1),
   .CONTEXT_WR_EN_I(write_enable_context_pe1),
   .CONTEXT_WR_ADDR_I(context_addr_pe1),.AMIDAR_I(parameter_data),
   .PREDICATION_I(w_predication),
   .DIRECT_O(w_direct_out_1),
   .LIVEOUT_O(w_pe_out_1) );


 //  wire [32-1:0] context_data_pe2;
wire [8-1:0] context_addr_pe2;
wire write_enable_context_pe2;
wire [32-1:0] preped_context_pe2;

assign context_data_pe2 = ctrl_parameterEn_pe2 ? preped_context_pe2 : parameter_update_oneMoreCycle ? 0 : context_data[31:0];
assign context_addr_pe2 = parameter_update_oneMoreCycle || ctrl_parameterEn_pe2 ? {8{1'b1}} : context_addr[CONTEXT_ADDR_WIDTH-1:0];
assign write_enable_context_pe2 = (ctrl_contextEn_pe2 && ~is_peLog_context) || ctrl_parameterEn_pe2 || parameter_update_oneMoreCycle;
assign preped_context_pe2 = { {1'b1},{1'b0},{1'b1},{12'b0},{rf_wr_addr[5-1:0]},{4'b0},{1'b1},{4'b0} };



   PE_2 pe_2( 
   .CLK_I(CLK_I),.EN_I(enable_datapath_modules_sync),
   .CCNT_I(w_ccnt), 
   .INPUT_0_I(w_direct_out_0),
   .INPUT_3_I(w_direct_out_3),
   .CONTEXT_EN_I(w_en_pe_context),
   .CONTEXT_DATA_I(context_data_pe2),
   .CONTEXT_WR_EN_I(write_enable_context_pe2),
   .CONTEXT_WR_ADDR_I(context_addr_pe2),.AMIDAR_I(parameter_data),
   .PREDICATION_I(w_predication),
   .DIRECT_O(w_direct_out_2),
   .LIVEOUT_O(w_pe_out_2) );


 //  wire [35-1:0] context_data_pe3;
wire [8-1:0] context_addr_pe3;
wire write_enable_context_pe3;
wire [35-1:0] preped_context_pe3;

assign context_data_pe3 = ctrl_parameterEn_pe3 ? preped_context_pe3 : parameter_update_oneMoreCycle ? 0 : context_data[34:0];
assign context_addr_pe3 = parameter_update_oneMoreCycle || ctrl_parameterEn_pe3 ? {8{1'b1}} : context_addr[CONTEXT_ADDR_WIDTH-1:0];
assign write_enable_context_pe3 = (ctrl_contextEn_pe3 && ~is_peLog_context) || ctrl_parameterEn_pe3 || parameter_update_oneMoreCycle;
assign preped_context_pe3 = { {1'b1},{1'b0},{1'b1},{14'b0},{rf_wr_addr[6-1:0]},{4'b0},{1'b1},{4'b0} };



   PE_3 pe_3( 
   .CLK_I(CLK_I),
   .RST_I(RST_I), .EN_I(enable_datapath_modules_sync),
   .CCNT_I(w_ccnt), 
   .INPUT_1_I(w_direct_out_1),
   .INPUT_2_I(w_direct_out_2),
   .CONTEXT_EN_I(w_en_pe_context),
   .CONTEXT_DATA_I(context_data_pe3),
   .CONTEXT_WR_EN_I(write_enable_context_pe3),
   .CONTEXT_WR_ADDR_I(context_addr_pe3),.AMIDAR_I(parameter_data),
   .PREDICATION_I(w_predication),
   .DIRECT_O(w_direct_out_3) ,
   .ALU_STATUS_O(w_status_3));


 //  

endmodule
