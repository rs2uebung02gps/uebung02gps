`ifndef INCLUDE_CGRA
`define INCLUDE_CGRA

  `define 	CGRA_CONTEXT_SIZE 256
  `define 	CGRA_CONTEXT_ADDR_WIDTH 8
  `define 	PE_COUNT 4
  `define 	PE_ID_WIDTH 2
  `define 	RF_ADDR_WIDTH 6

	`define   DATA_WIDTH 32
  `define 	ERROR_WIDTH 1

  // state definitions 
  `define 	STATE_WIDTH 3
	`define 	IDLE 0
	`define 	START 1
	`define 	EXECUTE 2
	`define 	UPDATE_PARAMETER 3
	`define 	ERROR 4

`endif // INCLUDE_CGRA
