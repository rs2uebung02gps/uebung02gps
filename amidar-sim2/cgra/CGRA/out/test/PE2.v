/*
Author: Dennis L. Wolf
Date: Mon May 29 13:28:06 CEST 2017
Version: 2.0 (Changed to String Template Interface)
Version History: 1.2 Code Review and Cleanup
		 1.1 debugged and tested
		 1.0 Concept
*/

`include "cgra.vh" // import definitions of parameters and types

`default_nettype wire

module PE_2
  ( 
  input wire CLK_I, input wire EN_I,
  input wire [8-1:0] CCNT_I,
  input wire [8-1:0]  CONTEXT_WR_ADDR_I,
  input wire [31:0] CONTEXT_DATA_I,
  input wire CONTEXT_EN_I,
  input wire CONTEXT_WR_EN_I,
  input wire [32-1:0]	INPUT_0_I,
  input wire [32-1:0]	INPUT_3_I,

  input wire [32-1:0] AMIDAR_I,
  input wire [6-1:0] PREDICATION_I,
  output wire [32-1:0] DIRECT_O ,
  output wire [`DATA_WIDTH-1:0] LIVEOUT_O  );

// Possible Sources for the input of the register file

  parameter	LIVE_IN = 1, ALU = 0;

// Wires instanciation
wire [32-1:0] w_reg_to_operand_mux;
reg [32-1:0] w_alu_in_A;
reg [32-1:0] w_alu_in_B; wire [32-1:0] w_alu_Out;

assign LIVEOUT_O = w_alu_in_B[32-1:0];

// Context memory - holds configurations of the PE
 (* ram_style = "block" *) reg [31:0] contextmemory [256-1:0];
reg [31:0] contextmemoryout;

// Context decoding

// Enable
wire w_enable_context;
assign w_enable_context = contextmemoryout[31];
// Marker whether a register write is predicated
wire w_conditional_write;
assign w_conditional_write = contextmemoryout[30];

// Write enable for the register file
wire w_write_enable;
assign w_write_enable = contextmemoryout[29];

// Address to load data from the register file that is routed to the multiplexer that picks the operand of the ALU
wire [6-1:0] w_rf_addr_operand_mux;
assign w_rf_addr_operand_mux = contextmemoryout[28:23];

// Address to load data from the register file that is routed directly to interconnect
wire [6-1:0] w_directout_addr;
assign w_directout_addr = contextmemoryout[22:17];

// Write address to the register file
wire [5-1:0] w_write_addr;
assign w_write_addr = contextmemoryout[13:9];

// Driver to pick operand A from the interconnect or the register file
wire[2-1:0] w_muxA;
assign w_muxA = contextmemoryout[8:7];
// Driver to pick operand B from the interconnect or the register file
wire[2-1:0] w_muxB;
assign w_muxB = contextmemoryout[6:5];

// Driver to determine the input of the multiplexer - ALU out | Live In ( | Cache)
wire [1-1:0] w_MuxR;
assign w_MuxR = contextmemoryout[4:4];

wire [3-1:0] w_predication_select;
assign w_predication_select = contextmemoryout[16:14];



// Opcode - Determines the operation to be carried out by the ALU
wire [4-1:0] w_opcode;
assign w_opcode = contextmemoryout[3:0];

// Both multiplexors to which connects the Data input of the registerfile
always@(*)begin
(* full_case *)
 case(w_muxA)
0	: w_alu_in_A = INPUT_0_I;
1	: w_alu_in_A = INPUT_3_I;
2	: w_alu_in_A = w_reg_to_operand_mux;

 endcase
(* full_case *)
 case(w_muxB)
0	: w_alu_in_B = INPUT_0_I;
1	: w_alu_in_B = INPUT_3_I;
2	: w_alu_in_B = w_reg_to_operand_mux;

 endcase
end
// enable generation
 wire w_enable_local;
 and(w_enable_local,EN_I,w_enable_context);

// Multiplexor in front of registerfile
reg [32-1:0] w_reg_in;


// contextmanagment
always@(posedge CLK_I) begin
  if(CONTEXT_EN_I) begin
    contextmemoryout <= contextmemory[CCNT_I];
    if(CONTEXT_WR_EN_I) begin
      contextmemory[CONTEXT_WR_ADDR_I] <= CONTEXT_DATA_I;
    end 
  end
end

always@(*) begin
(* full_case *)
 case(w_MuxR)
   ALU	: w_reg_in = w_alu_Out;
   LIVE_IN	: w_reg_in = AMIDAR_I;
  endcase
end



reg predication_w;
 always@(*)
 case(w_predication_select)
 0: predication_w = PREDICATION_I[0];
 1: predication_w = PREDICATION_I[1];
 2: predication_w = PREDICATION_I[2];
 3: predication_w = PREDICATION_I[3];
 4: predication_w = PREDICATION_I[4];
 5: predication_w = PREDICATION_I[5];

 endcase

wire  w_write_enable_regfile;  
assign w_write_enable_regfile = (w_conditional_write) ? (predication_w && w_write_enable) : w_write_enable;

// INSTANCES:

Alu2 #(.OPCODE_WIDTH(4))
  alu_2 (
 
  .OPERAND_A    (w_alu_in_A),
  .OPERAND_B    (w_alu_in_B),
  .OPCODE_I     (w_opcode),     
  .RESULT_O  	(w_alu_Out));

wire[32-1:0] w_direct_o;
assign DIRECT_O = w_direct_o;
Registerfile_2 regfile_2(
  .CLK_I(CLK_I),
  .EN_I(w_enable_local),
  .RD_PORT_DIRECT_ADDR_I(w_directout_addr),
  .RD_PORT_DIRECT_O(w_direct_o),
  .RD_PORT_MUX_ADDR_I(w_rf_addr_operand_mux),
  .RD_PORT_MUX_O(w_reg_to_operand_mux),
  .WR_PORT_EN_I(w_write_enable_regfile),
  .WR_PORT_ADDR_I(w_write_addr),
  .WR_PORT_DATA_I(w_reg_in) );

endmodule
