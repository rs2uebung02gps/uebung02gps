`ifndef INCLUDE_CONSTBUF
`define INCLUDE_CONSTBUF

`include "ultrasynth.vh"
`include "cgra.vh"
`include "constbuf.vh"

module ConstBuf
(
input wire CGRA_CLK_I,
input wire RST_N_I,
input wire EN_I,
(* dont_touch = "true" *) input wire [1-1:0] READ_ENABLE_PE0_I,
(* dont_touch = "true" *) input wire [1-1:0] READ_ENABLE_PE1_I,
(* dont_touch = "true" *) input wire [1-1:0] READ_ENABLE_PE2_I,
(* dont_touch = "true" *) input wire [1-1:0] READ_ENABLE_PE3_I,
(* dont_touch = "true" *) input wire [5-1:0] READ_ADDR_PE0_I,
(* dont_touch = "true" *) input wire [5-1:0] READ_ADDR_PE1_I,
(* dont_touch = "true" *) input wire [5-1:0] READ_ADDR_PE2_I,
(* dont_touch = "true" *) input wire [5-1:0] READ_ADDR_PE3_I,
(* dont_touch = "true" *) output wire [32-1:0] DATA_PE0_O,
(* dont_touch = "true" *) output wire [32-1:0] DATA_PE1_O,
(* dont_touch = "true" *) output wire [32-1:0] DATA_PE2_O,
(* dont_touch = "true" *) output wire [32-1:0] DATA_PE3_O,

input wire WRITE_ENABLE_I,
input wire [`CONST_BUF_ADDR_WIDTH-1:0] WRITE_ADDR_I,
input wire [`DATA_WIDTH-1:0] DATA_I
);

Memory mem0 (
	.CGRA_CLK_I(CGRA_CLK_I),
	.RST_N_I(RST_N_I),
	.EN_I(EN_I),
	.WRITE_ENABLE_I(WRITE_ENABLE_I),
	.WRITE_ADDR_I(WRITE_ADDR_I),
	.DATA_I(DATA_I),
	.READ_ENABLE_I(READ_ENABLE_PE0_I),
	.READ_ADDR_I(READ_ADDR_PE0_I),
	.DATA_O(DATA_PE0_O)
);
Memory mem1 (
	.CGRA_CLK_I(CGRA_CLK_I),
	.RST_N_I(RST_N_I),
	.EN_I(EN_I),
	.WRITE_ENABLE_I(WRITE_ENABLE_I),
	.WRITE_ADDR_I(WRITE_ADDR_I),
	.DATA_I(DATA_I),
	.READ_ENABLE_I(READ_ENABLE_PE1_I),
	.READ_ADDR_I(READ_ADDR_PE1_I),
	.DATA_O(DATA_PE1_O)
);
Memory mem2 (
	.CGRA_CLK_I(CGRA_CLK_I),
	.RST_N_I(RST_N_I),
	.EN_I(EN_I),
	.WRITE_ENABLE_I(WRITE_ENABLE_I),
	.WRITE_ADDR_I(WRITE_ADDR_I),
	.DATA_I(DATA_I),
	.READ_ENABLE_I(READ_ENABLE_PE2_I),
	.READ_ADDR_I(READ_ADDR_PE2_I),
	.DATA_O(DATA_PE2_O)
);
Memory mem3 (
	.CGRA_CLK_I(CGRA_CLK_I),
	.RST_N_I(RST_N_I),
	.EN_I(EN_I),
	.WRITE_ENABLE_I(WRITE_ENABLE_I),
	.WRITE_ADDR_I(WRITE_ADDR_I),
	.DATA_I(DATA_I),
	.READ_ENABLE_I(READ_ENABLE_PE3_I),
	.READ_ADDR_I(READ_ADDR_PE3_I),
	.DATA_O(DATA_PE3_O)
);


endmodule

module Memory 
(
	input wire CGRA_CLK_I,
	input wire RST_N_I,
	input wire EN_I,
	input wire WRITE_ENABLE_I,
	input wire [`CONST_BUF_ADDR_WIDTH-1:0] WRITE_ADDR_I,
	input wire [`DATA_WIDTH-1:0] DATA_I,
	input wire READ_ENABLE_I,
	input wire [`CONST_BUF_ADDR_WIDTH-1:0] READ_ADDR_I,
	output wire [`DATA_WIDTH-1:0] DATA_O
);

(* ram_style = "block" *) reg [`DATA_WIDTH-1:0] mem [`CONST_BUF_SIZE-1:0];
reg [`DATA_WIDTH-1:0] mem_out;

assign DATA_O = mem_out;

always @(posedge CGRA_CLK_I) begin
	if (~RST_N_I) begin
		mem_out <= 0;
	end else if (EN_I) begin
		if (WRITE_ENABLE_I)
			mem[WRITE_ADDR_I] <= DATA_I;

		if (READ_ENABLE_I)
			mem_out <= mem[READ_ADDR_I];
	end
end

endmodule
`endif // INCLUDE_CONSTBUF
