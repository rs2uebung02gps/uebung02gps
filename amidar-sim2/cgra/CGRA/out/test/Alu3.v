/*
Author: Dennis L. Wolf
Date: Mon May 29 13:28:06 CEST 2017
Version: 1.3 (Changed to String Template Interface and ultrasynth)
Version History: 1.2 Code Review and Cleanup
		 1.1 debugged and simulated
		 1.0 layout & concept
*/
// `timescale 1 ns / 1 ps

`include "cgra.vh" // import definitions of parameters and types

`default_nettype wire

module Alu3 #( parameter OPCODE_WIDTH = 0)(
  input CLK_I,
    input wire RST_I,
    input wire EN_I, 
  input wire signed [32-1:0] OPERAND_A,
  input wire signed [32-1:0] OPERAND_B,output reg signed  [32-1:0] RESULT_O , 
  input wire [OPCODE_WIDTH-1:0] OPCODE_I,
  output reg STATUS_O
  );

// Listing of the opcodes for the available operations
parameter NOP = 0;
parameter IADD = 1;
parameter ISUB = 2;
parameter IMUL_2cyc = 3;
parameter IOR = 4;
parameter IAND = 5;
parameter IXOR = 6;
parameter ISHL = 7;
parameter ISHR = 8;
parameter IUSHR = 9;
parameter IFEQ = 10;
parameter IFNE = 11;
parameter IFLT = 12;
parameter IFGE = 13;
parameter IFGT = 14;
parameter IFLE = 15;


//Instanciation of the operator modules - for each available operation there is a distinct module.

// Module for the operation NOP
wire [32-1:0] result_NOP;

NOP module_NOP (
.OP_A_I(OPERAND_A[32-1: 0]),  .RESULT_O(result_NOP)
);
// Module for the operation IADD
wire [32-1:0] result_IADD;

IADD module_IADD (
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_IADD)
);
// Module for the operation ISUB
wire [32-1:0] result_ISUB;

ISUB module_ISUB (
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_ISUB)
);
// Module for the operation IMUL_2cyc
wire enable_IMUL_2cyc;
assign enable_IMUL_2cyc = ((OPCODE_I == IMUL_2cyc) && EN_I) ? 1'b1 : 1'b0;
wire [32-1:0] result_IMUL_2cyc;

IMUL_2cyc module_IMUL_2cyc (
.CLK_I(CLK_I),
.RST_N_I(!RST_I),
.EN_I(enable_IMUL_2cyc), 
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_IMUL_2cyc)
);
// Module for the operation IOR
wire [32-1:0] result_IOR;

IOR module_IOR (
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_IOR)
);
// Module for the operation IAND
wire [32-1:0] result_IAND;

IAND module_IAND (
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_IAND)
);
// Module for the operation IXOR
wire [32-1:0] result_IXOR;

IXOR module_IXOR (
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_IXOR)
);
// Module for the operation ISHL
wire [32-1:0] result_ISHL;

ISHL module_ISHL (
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_ISHL)
);
// Module for the operation ISHR
wire [32-1:0] result_ISHR;

ISHR module_ISHR (
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_ISHR)
);
// Module for the operation IUSHR
wire [32-1:0] result_IUSHR;

IUSHR module_IUSHR (
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .RESULT_O(result_IUSHR)
);
// Module for the operation IFEQ
IFEQ module_IFEQ ( 
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .STATUS_O(status_IFEQ)
);
// Module for the operation IFNE
IFNE module_IFNE ( 
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .STATUS_O(status_IFNE)
);
// Module for the operation IFLT
IFLT module_IFLT ( 
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .STATUS_O(status_IFLT)
);
// Module for the operation IFGE
IFGE module_IFGE ( 
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .STATUS_O(status_IFGE)
);
// Module for the operation IFGT
IFGT module_IFGT ( 
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .STATUS_O(status_IFGT)
);
// Module for the operation IFLE
IFLE module_IFLE ( 
.OP_A_I(OPERAND_A[32-1: 0]),  
.OP_B_I(OPERAND_B[32-1: 0]), .STATUS_O(status_IFLE)
);


/* This block contains the result connection. This is basically a big multiplexer that switches
   the wires Result, Cache Valid, Cache Write and Status. Depending on whether the PE contains 
   control flow or memory accesing operations the designated wiring appears */
always@(*) begin
  case (OPCODE_I)
   NOP: begin 
      
      STATUS_O = 1'b0;  
      RESULT_O = result_NOP;
   end

   IADD: begin 
      
      STATUS_O = 1'b0;  
      RESULT_O = result_IADD;
   end

   ISUB: begin 
      
      STATUS_O = 1'b0;  
      RESULT_O = result_ISUB;
   end

   IMUL_2cyc: begin 
      
      STATUS_O = 1'b0;  
      RESULT_O = result_IMUL_2cyc;
   end

   IOR: begin 
      
      STATUS_O = 1'b0;  
      RESULT_O = result_IOR;
   end

   IAND: begin 
      
      STATUS_O = 1'b0;  
      RESULT_O = result_IAND;
   end

   IXOR: begin 
      
      STATUS_O = 1'b0;  
      RESULT_O = result_IXOR;
   end

   ISHL: begin 
      
      STATUS_O = 1'b0;  
      RESULT_O = result_ISHL;
   end

   ISHR: begin 
      
      STATUS_O = 1'b0;  
      RESULT_O = result_ISHR;
   end

   IUSHR: begin 
      
      STATUS_O = 1'b0;  
      RESULT_O = result_IUSHR;
   end

   IFEQ: begin 
      
      STATUS_O = status_IFEQ; 
      RESULT_O = 0; 
   end

   IFNE: begin 
      
      STATUS_O = status_IFNE; 
      RESULT_O = 0; 
   end

   IFLT: begin 
      
      STATUS_O = status_IFLT; 
      RESULT_O = 0; 
   end

   IFGE: begin 
      
      STATUS_O = status_IFGE; 
      RESULT_O = 0; 
   end

   IFGT: begin 
      
      STATUS_O = status_IFGT; 
      RESULT_O = 0; 
   end

   IFLE: begin 
      
      STATUS_O = status_IFLE; 
      RESULT_O = 0; 
   end


   default: begin
	STATUS_O = 0;
	RESULT_O = OPERAND_B;
	end
  endcase
end
endmodule

