`include "axiinterface.vh"

module ComUnit #
(
parameter integer CONTEXT_ADDR_WIDTH = -1,
parameter integer RF_ADDR_WIDTH = -1, // the max RF address width (out of all PEs)
parameter integer OTHERID_WIDTH = -1,
parameter integer PEID_WIDTH = -1,
parameter integer IDC_SIZE = 256,
parameter integer IDC_ADDR_WIDTH = 8,
parameter integer IDC_WIDTH = PEID_WIDTH + RF_ADDR_WIDTH,
parameter integer C_S_AXI_DATA_WIDTH	= 32,
parameter integer C_S_AXI_ADDR_WIDTH	= 32
)
(
/* S_AXI I/O start */
input wire  S_AXI_ACLK_I,
input wire  CGRA_CLK,
input wire  RST_N_I,
input wire  EN_I,
input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR_I,
input wire [7 : 0] S_AXI_AWLEN_I,
input wire [2 : 0] S_AXI_AWSIZE_I,
input wire [1 : 0] S_AXI_AWBURST_I,
input wire  S_AXI_AWVALID_I,
output wire  S_AXI_AWREADY_O,
input wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA_I,
input wire [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB_I,
input wire  S_AXI_WLAST_I,
input wire  S_AXI_WVALID_I,
output wire  S_AXI_WREADY_O,
output wire [1 : 0] S_AXI_BRESP_O,
output wire  S_AXI_BVALID_O,
input wire  S_AXI_BREADY_I,
input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR_I,
input wire [7 : 0] S_AXI_ARLEN_I,
input wire [2 : 0] S_AXI_ARSIZE_I,
input wire [1 : 0] S_AXI_ARBURST_I,
input wire  S_AXI_ARVALID_I,
output wire  S_AXI_ARREADY_O,
output wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA_O,
output wire [1 : 0] S_AXI_RRESP_O,
output wire  S_AXI_RLAST_O,
output wire  S_AXI_RVALID_O,
input wire  S_AXI_RREADY_I,
/* S_AXI I/O end */

/* WriteControl output start */
output wire  ENABLE_WRITE_CTRL_O, // enables the WriteControl module
output wire  VALID_CONTEXT_O, // declares that enough context data has been accumulated to write contexts > 32 bit
output wire [CONTEXT_ADDR_WIDTH-1:0] CONTEXT_ADDR_O, // the context address to write to
output wire [IDC_ADDR_WIDTH-1:0] AFFECTED_MODULE_O, // specifies the affected Module, PEs and "others" alike
output wire [IDC_WIDTH-1:0] PARAMETER_BUFFER_DESTINATION_O, // specifies the destination of a parameter, stored in the parameter buffer until it is written to its final destination
output wire [`OP_WIDTH-1:0] OPERATION_O, // specifies what is to be done: write contexts/parameters ...
/* WriteControl output end */

/* data output start */
output wire [C_S_AXI_DATA_WIDTH-1:0] DATA_0_O, // + further ports if context width > 32 bit
output wire [C_S_AXI_DATA_WIDTH-1:0] DATA_1_O,

/* data output end */

/* feedback inputs */
input wire 	WR_EN_IDC_I,
input wire 	PARAMETER_BUFFER_FULL_I,
input wire 	PARAMETER_CLEANUP_I
);
/* --- S_AXI internal signals start --- */

// AXI4FULL signals
wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR;
wire [7 : 0] S_AXI_AWLEN;
wire [2 : 0] S_AXI_AWSIZE;
wire [1 : 0] S_AXI_AWBURST;
wire  S_AXI_AWVALID;
wire  S_AXI_AWREADY;
wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA;
wire [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB;
wire  S_AXI_WLAST;
wire  S_AXI_WVALID;
wire  S_AXI_WREADY;
wire [1 : 0] S_AXI_BRESP;
wire  S_AXI_BVALID;
wire  S_AXI_BREADY;
wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR;
wire [7 : 0] S_AXI_ARLEN;
wire [2 : 0] S_AXI_ARSIZE;
wire [1 : 0] S_AXI_ARBURST;
wire  S_AXI_ARVALID;
wire  S_AXI_ARREADY;
wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA;
wire [1 : 0] S_AXI_RRESP;
wire  S_AXI_RLAST;
wire  S_AXI_RVALID;
wire  S_AXI_RREADY;

reg  	axi_awready;
reg  	axi_wready;
reg [1 : 0] 	axi_bresp;
reg  	axi_bvalid;
reg [C_S_AXI_ADDR_WIDTH-1 : 0] 	axi_araddr;
reg  	axi_arready;
reg [1 : 0] 	axi_rresp;
reg  	axi_rlast;
reg  	axi_rvalid;
reg [C_S_AXI_DATA_WIDTH-1 : 0] 	axi_rdata;

//The axi_arv_arr_flag flag marks the presence of read address valid
reg axi_arv_arr_flag; 
//The axi_arlen_cntr internal read address counter to keep track of beats in a burst transaction
reg [7:0] axi_arlen_cntr;
reg [1:0] axi_arburst;
reg [7:0] axi_arlen;
reg [7:0] axi_awlen;

// I/O Connections assignments

assign S_AXI_AWREADY = axi_awready;
assign S_AXI_WREADY	= axi_wready;
assign S_AXI_BRESP	= axi_bresp;
assign S_AXI_BVALID	= axi_bvalid;
assign S_AXI_ARREADY = axi_arready;
assign S_AXI_RDATA	= axi_rdata;
assign S_AXI_RRESP	= axi_rresp;
assign S_AXI_RLAST	= axi_rlast;
assign S_AXI_RVALID	= axi_rvalid;

/* --- S_AXI internal signals end --- */

parameter integer IDC_CNTX_ADDR_WIDTH = IDC_ADDR_WIDTH + CONTEXT_ADDR_WIDTH;

/* --- data --- */
reg [C_S_AXI_DATA_WIDTH-1:0] filtered; // incoming data, filtered by using the write strobe signal
reg [C_S_AXI_DATA_WIDTH-1:0] data_0; // output to be written to contexts
reg [C_S_AXI_DATA_WIDTH-1:0] data_1;


/* --- internal burst handling --- */
// incoming burst infos, used to check for currently invalid operations 
reg [8-1:0]                   incoming_len; 
reg [2-1:0]                   incoming_type;
reg [CONTEXT_ADDR_WIDTH-1:0] 	incoming_addr;
reg [IDC_ADDR_WIDTH-1:0] 			incoming_id;
reg [`OP_WIDTH-1:0]          	incoming_op;

// current burst infos, burst_addr or burst_id may be manipulated during the transaction processing
reg [8-1:0]                   burst_len; 
reg [2-1:0]                   burst_type;
reg [CONTEXT_ADDR_WIDTH-1:0]  burst_addr; // address for writing to context memories
reg [IDC_ADDR_WIDTH-1:0]			burst_id; // address for reading the idc
reg [`OP_WIDTH-1:0]           burst_op;

reg 	burst_hold; // latches the inverted valid_context	signal and forces an address change low
reg 	valid_context; // is only high if there is enough data available to write one context entry of the current transaction target
reg [8-1:0] transfer_cntr; // default 0, tracks (for the current burst) the amount of transfers already done
reg 	burst_underway; // states that there was a valid (AWVALID) address for the current burst
reg 	ctrl_was_triggered; /* stores the last value of enable_write_ctrl to check if write control was enabled */
reg 	[1-1:0] valid_data_cntr; /* counts how many transfers were done to evaluate if there is enough data for a context write */
wire 	enable_write_ctrl; // enables the Write_Control module
wire 	hold_transactions; // collects all conditions which force the slave to not take in any more data (for the time being)
wire 	allowed_to_proceed; // indicates that a burst is allowed to be carried out
wire 	burst_en_wrap; // wrap enable for the wrapping AXI burst

assign burst_en_wrap = ((burst_addr & burst_len) == burst_len) ? 1'b1 : 1'b0;

/* --- idContext --- */
 (* ram_style = "block" *) reg [IDC_WIDTH-1:0] idContext [IDC_SIZE-1:0];
reg [IDC_WIDTH-1:0] idContext_out; // idc output helper signal, in sync with data_0

/* --- output assignments --- */
assign ENABLE_WRITE_CTRL_O = enable_write_ctrl;
assign VALID_CONTEXT_O = valid_context;
assign CONTEXT_ADDR_O = burst_addr;
assign AFFECTED_MODULE_O = burst_id;
assign PARAMETER_BUFFER_DESTINATION_O = idContext_out;
assign OPERATION_O = burst_op; // for buffered bursts, following directly after another burst
assign DATA_0_O = data_0; // + further assignments if context width > 32 bit
assign DATA_1_O = data_1;


/* --- handle write strobes and unaligned data --- */

/*
 * Does not support unaligned 4/2 byte transfers,
 */
always @(*) begin
	filtered[32-1:0] = 0;
	if (S_AXI_AWSIZE == 3'b000) // 1 byte transfer
		filtered[8-1:0] = S_AXI_WSTRB[0] ? S_AXI_WDATA[8-1:0] :
											S_AXI_WSTRB[1] ? S_AXI_WDATA[16-1:8] :
											S_AXI_WSTRB[2] ? S_AXI_WDATA[24-1:16] : S_AXI_WDATA[32-1:24];
	else if (S_AXI_AWSIZE == 3'b001) // 2 byte transfer
		filtered[16-1:0] = S_AXI_WSTRB[0] ? S_AXI_WDATA[16-1:0] : 
											 S_AXI_WSTRB[2] ? S_AXI_WDATA[24-1:8] : S_AXI_WDATA[32-1:16];
	else 
		filtered[32-1:0] = S_AXI_WDATA[32-1:0]; // 4 byte transfer
end

/* --- handle incoming and internal data/info ------------- */

// data and start address latching
always @(posedge CGRA_CLK) begin
	if (~RST_N_I) begin
		data_0 <= 0;
		ctrl_was_triggered <= 1'b0;
	end else if (EN_I) begin
		// data
		if (enable_write_ctrl)
			data_0 <= filtered;

			ctrl_was_triggered <= enable_write_ctrl;

		/* helps with changing intervals between ops 
		 * (e.g. fast reading from the buffer but suddenly having to wait for new arrivals) 
		 * ctrl_was_triggered actually implicates that data was available */
		if (ctrl_was_triggered) begin
			data_1 <= data_0;
		end
	end
end


/* This block handles incoming information, prepares the next burst
 * or increments the address used in the current burst. The process works like this: 
 * AWVALID HIGH ->  save all needed information in incoming_*, save AWVALID to burst_underway
 * during the next cycle -> generate allowed_to_proceed
 * at the next rising clock edge -> write incoming_* to burst_*, set the WriteControl enable based on WVALID and allowed_to_proceed */
always @(posedge CGRA_CLK) begin
	if (RST_N_I == 1'b0) begin
		incoming_len <= 8'hff;
		incoming_type <= 0;
		incoming_op <= {`OP_WIDTH{1'b0}};
		incoming_addr <= {CONTEXT_ADDR_WIDTH{1'b1}};
		incoming_id <= 0;
		burst_len <= 8'hff;
		burst_type <= 0;
		burst_op <= {`OP_WIDTH{1'b0}};
		burst_addr <= {CONTEXT_ADDR_WIDTH{1'b1}};
		burst_id <= 0;
	end 
	else if (EN_I) begin
		if (S_AXI_AWVALID && S_AXI_AWREADY) begin
			incoming_len <= S_AXI_AWLEN;
			incoming_type <= S_AXI_AWBURST;
			incoming_addr <= S_AXI_AWADDR[CONTEXT_ADDR_WIDTH-1:0];
			incoming_op <= S_AXI_AWADDR[3-1+IDC_CNTX_ADDR_WIDTH:IDC_CNTX_ADDR_WIDTH];
			incoming_id <= S_AXI_AWADDR[IDC_CNTX_ADDR_WIDTH-1:CONTEXT_ADDR_WIDTH];
		end

		// if no burst is running => get new info
		if (transfer_cntr == 0 && burst_underway) begin 
				burst_len <= incoming_len;
				burst_type <= incoming_type;
				burst_addr <= incoming_addr; // context write start offset
				burst_op <= incoming_op; // operation
				burst_id <= incoming_id; // idContext index
		end
		else if (enable_write_ctrl) begin
			if (burst_op[1]) begin // iterate over the idContext if writing locals
				if (burst_type == 2'b00)
					burst_id <= burst_id; 
				else if ((burst_type == 2'b10) && burst_en_wrap)
					burst_id <= burst_id - burst_len;
				else
					burst_id <= burst_id + 1; 
			end
			else begin // generate context write addresses
				if (burst_type == 2'b00 || burst_hold)
					burst_addr <= burst_addr; // hold the address while waiting for more data (contexts > 32 bit)
				else if ((burst_type == 2'b10) && burst_en_wrap)
					burst_addr <= burst_addr - burst_len;
				else
					burst_addr <= burst_addr + 1; 
			end
		end
	end // if(EN_I)
end

/* --- internal burst management ------------------- */

/* This block generates the 'valid_context' signal.
 * The signals value is driven to high if the last bits of 'valid_data_cntr' match the context length
 * of a context counted in 32 bit steps, starting at 0 (meaning a size <= 32 bits). 
 * This block uses the template validContextGenLine, located at the EOF */
always @(*) begin
	case(burst_id[OTHERID_WIDTH-1:0])
	1:
		if (burst_op[0]) valid_context = valid_data_cntr[1-1:0] == 1;
		else valid_context = 1'b1;
	3:
		if (burst_op[0]) valid_context = 1'b1;
		else valid_context = burst_id[IDC_ADDR_WIDTH-1] ? 1'b1 : valid_data_cntr[1-1:0] == 1;

	default: valid_context = 1'b1;
	endcase
end

always @(posedge CGRA_CLK) begin
	if (RST_N_I == 1'b0) begin
		transfer_cntr <= 0;
		burst_hold <= 1'b1;
		valid_data_cntr <= 0;
	end
	else if (EN_I && enable_write_ctrl) begin
		burst_hold <= ~valid_context;

		if (transfer_cntr == burst_len) begin // prepare the next burst, or ...
			transfer_cntr <= 0;
			valid_data_cntr <= 0;
		end else if (burst_underway) begin // ... continue with next transfer of the current burst
			transfer_cntr <= transfer_cntr + 1;

			if (valid_context)
				valid_data_cntr <= 0;
			else
				valid_data_cntr <= valid_data_cntr + 1;
		end
	end
end


/* --- AXI based enable -------------------------------------------------- */

/*
 * Checks incoming burst information for a currently invalid operation. 
 * (e.g. a full parameter buffer while writing parameters).
 */
assign hold_transactions = !(incoming_op == `WRITE_PARAMETER && PARAMETER_BUFFER_FULL_I)
												&& !(incoming_op == `WRITE_CONTEXT_PE && ~incoming_id[IDC_ADDR_WIDTH-1] && PARAMETER_CLEANUP_I);

/*
 * Is only asserted if there was a asserted AWVALID for this burst 
 * (stored in "burst_underway" until WLAST) without an hold request.
 */
assign allowed_to_proceed = burst_underway && hold_transactions && !(enable_write_ctrl && S_AXI_WLAST);

/*
 * Availability of data and actually being ready to take in data marks the moment in which 
 * WriteControl will be enabled.
 */
assign enable_write_ctrl = S_AXI_WVALID && S_AXI_WREADY;

/* 
 * Generates the WriteControl enable signal while keeping track of if there
 * was a valid address for the current transaction.
 */
always @(posedge CGRA_CLK) begin
	if (RST_N_I == 1'b0) begin
		burst_underway <= 1'b0;
	end else if (EN_I) begin
		if (S_AXI_AWVALID && S_AXI_AWREADY)
			burst_underway <= 1'b1;
		else if (enable_write_ctrl && S_AXI_WLAST)
			burst_underway <= 1'b0;
	end
end

/* --- idContext read and write -------------------------------------------------- */

always @(posedge CGRA_CLK) begin
	if (EN_I) begin
		if (WR_EN_IDC_I)
			idContext[burst_addr[IDC_ADDR_WIDTH-1:0]] <= data_0[IDC_WIDTH-1:0];
		idContext_out <= idContext[burst_id];
	end
end

/* --- S_AXI outside protocol handling start --- */

// Implement axi_awready generation
always @( posedge CGRA_CLK )
begin
  if ( RST_N_I == 1'b0 )
    begin
      axi_awready <= 1'b0;
      axi_bresp <= 2'b0;
    end 
  else if (EN_I)
    begin
  		axi_awready <= !(axi_awready && S_AXI_AWVALID) && ~burst_underway;

      if (S_AXI_AWVALID)
        begin
         	axi_bresp <= 2'b00; // OKAY
        end
    end 
end  

// Implement axi_wready generation
always @( posedge CGRA_CLK )
begin
  if (RST_N_I == 1'b0)
    begin
    	axi_wready <= 1'b0;
    end 
  else if (EN_I)
  	begin
  		axi_wready <= allowed_to_proceed;
  	end 
end       

// Implement write response logic generation
always @( posedge CGRA_CLK )
begin
  if ( RST_N_I == 1'b0 )
    begin
      axi_bvalid <= 0;  
    end 
  else
    begin    
      if (axi_wready && S_AXI_WVALID && S_AXI_WLAST && EN_I)
        begin
          axi_bvalid <= 1'b1; 
        end                   
      else
        begin
          if (S_AXI_BREADY && axi_bvalid) 
          //check if bready is asserted while bvalid is high) 
          //(there is a possibility that bready is always asserted high)   
            begin
              axi_bvalid <= 1'b0; 
            end  
        end
    end
 end   


// Implement axi_arready generation
always @( posedge CGRA_CLK )
begin
  if ( RST_N_I == 1'b0 )
    begin
      axi_arready <= 1'b0;
      axi_arv_arr_flag <= 1'b0;
    end 
  else
    begin    
      if (~axi_arready && S_AXI_ARVALID && ~axi_arv_arr_flag)
        begin
          axi_arready <= 1'b1;
          axi_arv_arr_flag <= 1'b1;
        end
      else if (axi_rvalid && S_AXI_RREADY && axi_arlen_cntr == axi_arlen)
      // preparing to accept next address after current read completion
        begin
          axi_arv_arr_flag  <= 1'b0;
        end
      else        
        begin
          axi_arready <= 1'b0;
        end
    end 
end    

// Implement axi_araddr latching
always @( posedge CGRA_CLK )
begin
  if ( RST_N_I == 1'b0 )
    begin
      axi_araddr <= 0;
      axi_arlen_cntr <= 0;
      axi_arburst <= 0;
      axi_arlen <= 0;
      axi_rlast <= 1'b0;
    end 
  else
    begin    
      if (~axi_arready && S_AXI_ARVALID && ~axi_arv_arr_flag)
        begin
          // address latching 
          axi_araddr <= S_AXI_ARADDR[C_S_AXI_ADDR_WIDTH - 1:0]; 
          axi_arburst <= S_AXI_ARBURST; 
          axi_arlen <= S_AXI_ARLEN;     
          // start address of transfer
          axi_arlen_cntr <= 0;
          axi_rlast <= 1'b0;
        end   
      else if((axi_arlen_cntr <= axi_arlen) && axi_rvalid && S_AXI_RREADY)        
        begin
          axi_arlen_cntr <= axi_arlen_cntr + 1;
          axi_rlast <= 1'b0;
          // omitted all further burst address handling
        end
      else if((axi_arlen_cntr == axi_arlen) && ~axi_rlast && axi_arv_arr_flag )   
        begin
          axi_rlast <= 1'b1;
        end          
      else if (S_AXI_RREADY)   
        begin
          axi_rlast <= 1'b0;
        end          
    end 
end  

// Implement axi_arvalid generation
always @( posedge CGRA_CLK )
begin
  if ( RST_N_I == 1'b0 )
    begin
    	axi_rdata <= 0;
      axi_rvalid <= 0;
      axi_rresp  <= 0;
    end 
  else
    begin    
      if (axi_arv_arr_flag && ~axi_rvalid)
        begin
        	axi_rdata <= 32'hdead_c0de;
          axi_rvalid <= 1'b1;
          axi_rresp  <= 2'b10; // SLVERR for all reads
        end   
      else if (axi_rvalid && S_AXI_RREADY)
        begin
        	axi_rdata <= 0;
          axi_rvalid <= 1'b0;
        end            
    end
end

// --- CDC FIFO master interface wires and instantiation

// the sole purpose of these wires is connecting the AXI FIFO to internally used wires
wire [C_S_AXI_ADDR_WIDTH-1:0] m_axi_awaddr;
wire [8-1:0] m_axi_awlen;
wire [3-1:0] m_axi_awsize;
wire [2-1:0] m_axi_awburst;
wire m_axi_awvalid;
wire m_axi_awready;
wire [C_S_AXI_DATA_WIDTH-1:0] m_axi_wdata;
wire [(C_S_AXI_DATA_WIDTH/8)-1:0] m_axi_wstrb;
wire m_axi_wlast;
wire m_axi_wvalid;
wire m_axi_wready;
wire [2-1:0] m_axi_bresp;
wire m_axi_bvalid;
wire m_axi_bready;
wire [C_S_AXI_ADDR_WIDTH-1:0] m_axi_araddr;
wire [8-1:0] m_axi_arlen;
wire [3-1:0] m_axi_arsize;
wire [2-1:0] m_axi_arburst;
wire m_axi_arvalid;
wire m_axi_arready;
wire [C_S_AXI_DATA_WIDTH-1:0] m_axi_rdata;
wire [2-1:0] m_axi_rresp;
wire m_axi_rlast;
wire m_axi_rvalid;
wire m_axi_rready;

assign S_AXI_AWADDR = m_axi_awaddr;
assign S_AXI_AWLEN = m_axi_awlen;
assign S_AXI_AWSIZE = m_axi_awsize;
assign S_AXI_AWBURST = m_axi_awburst;
assign S_AXI_AWVALID = m_axi_awvalid;
assign m_axi_awready = S_AXI_AWREADY && EN_I;
assign S_AXI_WDATA = m_axi_wdata;
assign S_AXI_WSTRB = m_axi_wstrb;
assign S_AXI_WLAST = m_axi_wlast;
assign S_AXI_WVALID = m_axi_wvalid;
assign m_axi_wready = S_AXI_WREADY && EN_I;
assign m_axi_bresp = S_AXI_BRESP;
assign m_axi_bvalid = S_AXI_BVALID;
assign S_AXI_BREADY = m_axi_bready;
assign S_AXI_ARADDR = m_axi_araddr;
assign S_AXI_ARLEN = m_axi_arlen;
assign S_AXI_ARSIZE = m_axi_arsize;
assign S_AXI_ARBURST = m_axi_arburst;
assign S_AXI_ARVALID = m_axi_arvalid;
assign m_axi_arready = S_AXI_ARREADY;
assign m_axi_rdata = S_AXI_RDATA;
assign m_axi_rresp = S_AXI_RRESP;
assign m_axi_rlast = S_AXI_RLAST;
assign m_axi_rvalid = S_AXI_RVALID;
assign S_AXI_RREADY = m_axi_rready;

axi_cdc_fifo cdc_fifo (
  .m_aclk(CGRA_CLK),
  .s_aclk(S_AXI_ACLK_I),
  .s_aresetn(RST_N_I),
  .s_axi_awaddr(S_AXI_AWADDR_I),
  .s_axi_awlen(S_AXI_AWLEN_I),
  .s_axi_awsize(S_AXI_AWSIZE_I),
  .s_axi_awburst(S_AXI_AWBURST_I),
  .s_axi_awlock(1'b0),
  .s_axi_awcache(4'b0),
  .s_axi_awprot(3'b010),
  .s_axi_awqos(4'b0),
  .s_axi_awregion(4'b0),
  .s_axi_awvalid(S_AXI_AWVALID_I),
  .s_axi_awready(S_AXI_AWREADY_O),
  .s_axi_wdata(S_AXI_WDATA_I),
  .s_axi_wstrb(S_AXI_WSTRB_I),
  .s_axi_wlast(S_AXI_WLAST_I),
  .s_axi_wvalid(S_AXI_WVALID_I),
  .s_axi_wready(S_AXI_WREADY_O),
  .s_axi_bresp(S_AXI_BRESP_O),
  .s_axi_bvalid(S_AXI_BVALID_O),
  .s_axi_bready(S_AXI_BREADY_I),
  .m_axi_awaddr(m_axi_awaddr),
  .m_axi_awlen(m_axi_awlen),
  .m_axi_awsize(m_axi_awsize),
  .m_axi_awburst(m_axi_awburst),
  .m_axi_awlock(),
  .m_axi_awcache(),
  .m_axi_awprot(),
  .m_axi_awqos(),
  .m_axi_awregion(),
  .m_axi_awvalid(m_axi_awvalid),
  .m_axi_awready(m_axi_awready),
  .m_axi_wdata(m_axi_wdata),
  .m_axi_wstrb(m_axi_wstrb),
  .m_axi_wlast(m_axi_wlast),
  .m_axi_wvalid(m_axi_wvalid),
  .m_axi_wready(m_axi_wready),
  .m_axi_bresp(m_axi_bresp),
  .m_axi_bvalid(m_axi_bvalid),
  .m_axi_bready(m_axi_bready),
  .s_axi_araddr  (S_AXI_ARADDR_I),
  .s_axi_arlen   (S_AXI_ARLEN_I),
  .s_axi_arsize  (S_AXI_ARSIZE_I),
  .s_axi_arburst (S_AXI_ARBURST_I),
  .s_axi_arlock  (1'b0),
  .s_axi_arcache (4'b0),
  .s_axi_arprot  (3'b010),
  .s_axi_arqos   (4'b0),
  .s_axi_arregion(4'b0),
  .s_axi_arvalid (S_AXI_ARVALID_I),
  .s_axi_arready (S_AXI_ARREADY_O),
  .s_axi_rdata   (S_AXI_RDATA_O),
  .s_axi_rresp   (S_AXI_RRESP_O),
  .s_axi_rlast   (S_AXI_RLAST_O),
  .s_axi_rvalid  (S_AXI_RVALID_O),
  .s_axi_rready  (S_AXI_RREADY_I),
  .m_axi_araddr  (m_axi_araddr),
  .m_axi_arlen   (m_axi_arlen),
  .m_axi_arsize  (m_axi_arsize),
  .m_axi_arburst (m_axi_arburst),
  .m_axi_arlock  (),
  .m_axi_arcache (),
  .m_axi_arprot  (),
  .m_axi_arqos   (),
  .m_axi_arregion(),
  .m_axi_arvalid (m_axi_arvalid),
  .m_axi_arready (m_axi_arready),
  .m_axi_rdata   (m_axi_rdata),
  .m_axi_rresp   (m_axi_rresp),
  .m_axi_rlast   (m_axi_rlast),
  .m_axi_rvalid  (m_axi_rvalid),
  .m_axi_rready  (m_axi_rready)
);

/* --- S_AXI outside protocol handling end --- */

endmodule
