package gps.acquisition;

import cgra.pe.PETrigonometry;

/**
 * Im Rahmen der �bung soll der Akquisitionsalgorithmus eines GPS 
 * Empf�ngers nach zwei voneinander unabh�ngigen Aspekten f�r
 * den AMIDAR-Simulator optimiert werden.
 * 
 * Die zwei L�sungsaspekte:
 *    1. Maximale Performance: Hierbei soll der Algorithmus in m�glichst wenigen Zeitschritten durchgef�hrt werden.
 *    2. Minimaler Energiebedarf: Hierbei soll der Algorithmus m�glichst wenig Energie verbrauchen.
 * 
 * Reihenfolge der Aufrufe durch AcquisitionTest:
 *    1. Konstruktor
 *    2. F�r jede Smaplezeile einmal: "enterSample"
 *    3. F�r jede Smaplezeile einmal: "enterCode"
 *    4. "startAcquistion"
 *    5. "getDopplerverschiebung" und "getCodeVerschiebung" in einer Zeile
 * 
 * 
 * @author Lucas Schlestein und Kai Meinhard
 *
 */


public class Acquisition {
  
	private int nrOfSamples;
	private float gamma;
	private float[] realSample;
	private float[] imagSample;
	
	private float[] realCode;
	private float[] imagCode;
	
	private int currentPosSample;
	private int currentPosCode;
	
	private int dopplerVersch;
	private int codeVersch;
	
  /**
   * Konstruktor, der die Klasse mit Anzahl der Samples.
   * 
   * @param nrOfSamples Anzahl der Samples
   */
  
  public Acquisition(int nrOfSamples){
	  currentPosSample = 0;
	  currentPosCode = 0;
	  
	  this.nrOfSamples = nrOfSamples;
	  this.gamma = 0.015f;
	  
	  realSample = new float[nrOfSamples];
	  imagSample = new float[nrOfSamples];
	  realCode = new float[nrOfSamples];
	  imagCode = new float[nrOfSamples];
  }
  
  /**
   * Nimmt jeweils ein komplexes Sample entgegen.
   * Speichert dieses intern.
   * 
   * @param real Realteil des komplexen Samples
   * @param imag Imagin�rteil des komplexen Samples
   */
  
  public void enterSample(float real, float imag){
	  this.realSample[currentPosSample] = real;
	  this.imagSample[currentPosSample] = imag;
	  currentPosSample++;
  }
  
  /**
   * Nimmt jeweils einen komplexen Wert des L1 C/A Codes entgegen
   * 
   * @param real Realteil des komplexen Codes
   * @param imag Imagin�rteil des komplexen Codes
   */
  
  public void enterCode(float real, float imag){
	  this.realCode[currentPosCode] = real;
	  this.imagCode[currentPosCode] = imag;
	  currentPosCode++;
  }
  
  
  /**
   * F�hrt Akquisition aus
   * Gibt die Akquisitionsnachricht zur�ck
   * Speichert Doppler- und Codeverschiebung intern
   * 
   * http://fourier.eng.hmc.edu/e59/lectures/signalsystem/node18.html
   * 
   * Paper zu der gesamten Aquisition + Beispiel code
   * http://www.sm.luth.se/csee/courses/sms/019/1998/navstar/navstar.pdf
   * 
   * @return "True" wenn ein das Signal eines Satelliten empfangen wurde sonst "False"
   */
  
  	public boolean startAcquisition(){
  		float twoPI = 6.2831853f;
  		
  		int numberOfSteps = 11;
  		
  		int fd = -5000;
	  	int fs = 400000;
  		
	  	//Matrix R element C mxN
	  	
	  	float[][] solutionRealMatrix = new float[nrOfSamples][numberOfSteps];
	  	float[][] solutionImagMatrix = new float[nrOfSamples][numberOfSteps];
	  	
	  	float[] realDFT = new float[nrOfSamples];
  		float[] imagDFT = new float[nrOfSamples];
  		
	  	float[] realDFT_Code = new float[nrOfSamples];
	  	float[] imagDFT_Code = new float[nrOfSamples];
	  	
	  	float[] xfdReal = new float[nrOfSamples];
		float[] xfdImag = new float[nrOfSamples];
		
		float[] mulReal = new float[nrOfSamples];
    	float[] mulImag = new float[nrOfSamples];
	  	
  		// wir brauchen eine gesamte for schleife f�r das alles da wir das nur zeilenweise machen
  		for (int counter = 0; counter < numberOfSteps; counter++) {
  			//Kann lange dauern, deswegen die aktuelle frequency 
  			//System.out.println("Current frequency" + fd);
  			
	 		//e^-i*2pi*fd/fs
  			
  			float[] tmpReal = new float[16];
		  	float[] tmpImag = new float[16];
  		  	for (int i = 0; i < nrOfSamples / 16; i++) {
  		  		for (int k = 0; k < 16; k++) {
	  	    		tmpReal[k] = 0;
	  	    		tmpImag[k] = 0;
  		  		}
  		  		
  				for (int j = 0; j < 16; j++) {
  					tmpReal[j] = (realSample[i * 16+j]* PETrigonometry.cos(twoPI * (i*16+j) * fd / fs))+(imagSample[(i*16+j)]*PETrigonometry.sin(twoPI * (i*16+j) * fd / fs));
  	  				tmpImag[j] = (imagSample[(i*16+j)]* PETrigonometry.cos(twoPI * (i*16+j) * fd / fs))-(realSample[(i*16+j)] * PETrigonometry.sin(twoPI * (i*16+j) * fd / fs));
  	  				
				}
  				
  				
  				for (int j = 0; j < 16; j++) {
  					xfdReal[i * 16 + j] = tmpReal[j];
  					xfdImag[i * 16 + j] = tmpImag[j];
				}
  			}
  	  		
  	  		//https://www.nayuki.io/page/how-to-implement-the-discrete-fourier-transform
  	  		//FFT(Xn)
  	  		//FFT(C)*
  		  	
  	  		

  	  		float[] tmpReal1 = new float[16];
	    	float[] tmpImag1 = new float[16];

	    	float[] tmpReal2 = new float[16];
	    	float[] tmpImag2 = new float[16];
  	  		
  	   		
  	  	    for (int k = 0; k < nrOfSamples / 16; k++) {
  	  	    	for (int i = 0; i < 16; i++) {
  	  	    		tmpReal1[i] = 0;
  	  	    		tmpImag1[i] = 0;
    	
  	  	    		tmpReal2[i] = 0;
  	  	    		tmpImag2[i] = 0;
				}
  	  	    	
  	  	    	
  	  	    	
	  			for (int t = 0; t < nrOfSamples; t++) {
	  	        	for (int j = 0; j < 16; j++) {
	  	        		tmpReal1[j] +=  xfdReal[t] * PETrigonometry.cos(twoPI * (t) * (k* 16+j) / nrOfSamples) + xfdImag[(t)] * PETrigonometry.sin(twoPI * t * (k* 16+j) / nrOfSamples);
	  	        		tmpImag1[j] += -xfdReal[t] * PETrigonometry.sin(twoPI * (t) * (k* 16+j) / nrOfSamples) + xfdImag[(t)] * PETrigonometry.cos(twoPI * (t) * (k* 16+j) / nrOfSamples);
	  	        		
  	  	        		tmpReal2[j] +=  realCode[t] * PETrigonometry.cos(twoPI * t * (k* 16+j) / nrOfSamples)+ imagCode[t] * PETrigonometry.sin(twoPI * t * (k* 16+j) / nrOfSamples);
  	  	        		tmpImag2[j] += -realCode[t] * PETrigonometry.sin(twoPI * t * (k* 16+j) / nrOfSamples)+ imagCode[t] * PETrigonometry.cos(twoPI * t * (k* 16+j) / nrOfSamples);
	  	        	}
	  	        }
	  			
  	  	        for (int j = 0; j < 16; j++) {//Vielleicht das noch oben rein=
  	  	        	realDFT[(k* 16+j)] = tmpReal1[j];
  	  	        	imagDFT[(k* 16+j)] = tmpImag1[j];
  	  	        	
  	  	        	realDFT_Code[k* 16+j] = tmpReal2[j];
  	  	        	imagDFT_Code[k* 16+j] = -tmpImag2[j]; //Minus wegen Konjugiert
  	  	        }
  	  	        
  	  	    }
  	  		  	  		
  	  		//FFT(Xn)*FFT(C)*
  	  		for (int i = 0;i < nrOfSamples / 16; i++) {
  	  			for (int j = 0; j < 16; j++) {
  	  				mulReal[i* 16+j] = realDFT[i* 16+j]*realDFT_Code[i* 16+j]-imagDFT[i* 16+j]*imagDFT_Code[i* 16+j];
  	  				mulImag[i* 16+j]  = realDFT[i* 16+j]*imagDFT_Code[i* 16+j]+realDFT_Code[i* 16+j]*imagDFT[i* 16+j];
  	  			}
  	  			
  	  		}
  	  		
  	  		//IFFT
  	  		float[] tmpReal3 = new float[16];
	    	float[] tmpImag3 = new float[16];
  	  	    for (int k = 0; k < nrOfSamples / 16; k++) {
	  	    	for (int i = 0; i < 16; i++) {
  	  	    		tmpReal3[i] = 0;
  	  	    		tmpImag3[i] = 0;
				}
	  	    	
  	  	        for (int t = 0; t < nrOfSamples; t++) {
  	  	        	for (int j = 0; j < 16; j++) {
  	  	        		tmpReal3[j] += mulReal[t] * PETrigonometry.cos(twoPI * t * (k* 16+j)  / nrOfSamples) - mulImag[t] * PETrigonometry.sin(twoPI * t * (k* 16+j)  / nrOfSamples);
  	  	        		tmpImag3[j] += mulReal[t] * PETrigonometry.sin(twoPI * t * (k* 16+j)  / nrOfSamples) + mulImag[t] * PETrigonometry.cos(twoPI * t * (k* 16+j)  / nrOfSamples);
  	  	        	}
  	  	            
  	  	        }
  	  	        
  	  	        for (int j = 0; j < 16; j++) {
  	  	        	solutionRealMatrix[(k* 16+j)][counter] = tmpReal3[j] / nrOfSamples;
  	  	        	solutionImagMatrix[(k* 16+j)][counter] = tmpImag3[j] / nrOfSamples;
  	  	        }
  	  	    }
  	  		
  	  	    
  	  	    //N�chste Schrittweite
  	  	    fd += 1000;
  		} 
  	    
  	    
  		//TODO finde Maximalwert in Ergebnismatrix + speichere Doppler- und Codeverschiebung
  		
  		float sMax = 0;
  		dopplerVersch = 0;
  		codeVersch = 0;

  		
  		for (int i = 0; i < nrOfSamples; i++) {
			for (int j = 0; j < numberOfSteps; j++) {
				if((solutionRealMatrix[i][j] * solutionRealMatrix[i][j]) 
						+ (solutionImagMatrix[i][j] * solutionImagMatrix[i][j]) > sMax) {
					sMax = (solutionRealMatrix[i][j] * solutionRealMatrix[i][j]) 
							+ (solutionImagMatrix[i][j] * solutionImagMatrix[i][j]);
					dopplerVersch = j * 1000 - 5000;
					codeVersch = i;
				}
			}
		}
		
  		
  		//TODO berechne gesch�tzte Signalleistung
  		float sum = 0;
  		
  		for (int i = 0; i < realSample.length; i++) {
  			for (int j = 0; j < imagSample.length; j++) {
  				//float betragXin = realSample[i] * realSample[i] 
  				//		+ imagSample[j] * imagSample[j];
				sum += 	realSample[i] * realSample[i] 
  						+ imagSample[j] * imagSample[j];
			}
		}
  		
  		//sum = sum / nrOfSamples;
  		
  		//TODO normiere den Maximalwert mit der Signalleistung
  		
  		float grosGamma = sMax / (sum / nrOfSamples);
  		
  		//TODO entscheide, ob  normierter Maximalwert hoch genug ist
  		
  		if(grosGamma > gamma) {
  			return true;
  		} else {
  			return false;
  		}
  	}
  	
  	
  /**
   * Auslesen der Dopplerverschiebung
   * 
   * @return Dopplerverschiedung
   */
  
  public int getDopplerverschiebung(){
    return dopplerVersch;
  }
  
  
  /**
   * Auslesen der Codeverschiebung
   * 
   * @return Codeverschiebung
   */
  
  public int getCodeVerschiebung(){
    return codeVersch;
  }
  
  

}
