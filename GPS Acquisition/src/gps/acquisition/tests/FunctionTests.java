package gps.acquisition.tests;

import gps.acquisition.helper.*;
import gps.acquisition.util.ImaginaryNumber;
import junit.framework.TestCase;

public class FunctionTests extends TestCase {
	
	/**
	 * http://fourier.eng.hmc.edu/e59/lectures/signalsystem/node18.html
	 * 
	 */
	
	public void testDFT() {
		//Setup
		ImaginaryNumber[] signal = new ImaginaryNumber[8];
		signal[0] = new ImaginaryNumber();
		signal[1] = new ImaginaryNumber();
		signal[2] = new ImaginaryNumber(2, 0);
		signal[3] = new ImaginaryNumber(3, 0);
		signal[4] = new ImaginaryNumber(4, 0);
		signal[5] = new ImaginaryNumber();
		signal[6] = new ImaginaryNumber();
		signal[7] = new ImaginaryNumber();
		
		//Execution
		ImaginaryNumber[] methodSolution = DFT.computeDFT(signal);
		
		//Test the result
		ImaginaryNumber[] solution = new ImaginaryNumber[8];
		solution[0] = new ImaginaryNumber(3.18f, 0.0f);
		solution[1] = new ImaginaryNumber(-2.16f, -1.46f);
		solution[2] = new ImaginaryNumber(0.71f, 1.06f);
		solution[3] = new ImaginaryNumber(-0.66f, -0.04f);
		solution[4] = new ImaginaryNumber(1.06f, 0.0f);
		solution[5] = new ImaginaryNumber(-0.66f, 0.04f);
		solution[6] = new ImaginaryNumber( 0.71f, -1.06f);
		solution[7] = new ImaginaryNumber(-2.61f, 1.46f);
		
		for (int i = 0; i < solution.length; i++) {
			assertTrue("Real "+i+" Should be: "+solution[i].getRealPart()+" But Was: "+ methodSolution[i].getRealPart(), methodSolution[i].getRealPart() == solution[i].getRealPart());
			assertTrue("Imaginary "+i+"Should be: "+solution[i].getImaginaryPart()+" Was: "+ methodSolution[i].getRealPart(), methodSolution[i].getImaginaryPart() == solution[i].getImaginaryPart());
			
		}
	}
	
	public void testInverseDFT() {
		//Setup
		
		//Execution
		InverseDFT.computeIDFT(null);		
		
		//Test the result
		if(true) {
			produceNullPointer(null);
		}
		
	}
	
	public void testKonjugierteMatrix() {
		//Setup
		
		//Execution
		KonjugiertMatrix.computeKonjugierteMatrix(null);
				
		//Test the result
		if(true) {
			fail("TestFailure");
		}
	}
	
	public void testMatrixKonstanteMultiplizierer() {
		//Setup
		
		//Execution
		MatrixKonstanteMultiplizierer.computeMatrixKonstanteMultiplikation(null);		
		
		//Test the result
		
	}
	
	public void testMatrixMittelwert() {
		//Setup
		
		//Execution
		MatrixMittelwert.computeMatrixMittelwert(null);
				
		//Test the result
		
	}
	
	public void testMatrizenMultiplizierer() {
		//Setup
		
		//Execution
		MatrizenMultiplizierer.computeMatrixMuliplikation(null, null);
				
		//Test the result
		
	}
	
	public void testMaximalwertFinder() {
		//Setup
		
		//Execution
		MaximalwertFinder.findMaximalwert(null);
				
		//Test the result
		
	}
	
	public void testTransponierteMatrix() {
		//Setup
		
		//Execution
		TransponiertMatrix.computeTransponierteMatrix(null);
				
		//Test the result
		
	}
	
	public void testXinToXfd() {
		//Setup
		
		//Execution
		XinToXfd.computeXfd(null);
				
		//Test the result
		
	}
	
	
	public void produceNullPointer(String str) {
		char c = str.toCharArray()[3];
	}
	

}
