package gps.acquisition;

import gps.acquisition.helper.DFT;
import gps.acquisition.helper.InverseDFT;
import gps.acquisition.helper.KonjugiertMatrix;
import gps.acquisition.helper.MatrixKonstanteMultiplizierer;
import gps.acquisition.helper.MatrixMittelwert;
import gps.acquisition.helper.MatrizenMultiplizierer;
import gps.acquisition.helper.MaximalwertFinder;
import gps.acquisition.helper.TransponiertMatrix;
import gps.acquisition.helper.XinToXfd;

/**
 * Im Rahmen der �bung soll der Akquisitionsalgorithmus eines GPS 
 * Empf�ngers nach zwei voneinander unabh�ngigen Aspekten f�r
 * den AMIDAR-Simulator optimiert werden.
 * 
 * Die zwei L�sungsaspekte:
 *    1. Maximale Performance: Hierbei soll der Algorithmus in m�glichst wenigen Zeitschritten durchgef�hrt werden.
 *    2. Minimaler Energiebedarf: Hierbei soll der Algorithmus m�glichst wenig Energie verbrauchen.
 * 
 * Reihenfolge der Aufrufe durch AcquisitionTest:
 *    1. Konstruktor
 *    2. F�r jede Smaplezeile einmal: "enterSample"
 *    3. F�r jede Smaplezeile einmal: "enterCode"
 *    4. "startAcquistion"
 *    5. "getDopplerverschiebung" und "getCodeVerschiebung" in einer Zeile
 * 
 * 
 * @author Lucas Schlestein und Kai Meinhard
 *
 */


public class Acquisition {
  
  /**
   * Konstruktor, der die Klasse mit Anzahl der Samples.
   * 
   * @param nrOfSamples Anzahl der Samples
   */
  
  public Acquisition(int nrOfSamples){
	  
  }
  
  /**
   * Nimmt jeweils ein komplexes Sample entgegen.
   * Speichert dieses intern.
   * 
   * @param real Realteil des komplexen Samples
   * @param imag Imagin�rteil des komplexen Samples
   */
  
  public void enterSample(float real, float imag){
	  
  }
  
  /**
   * Nimmt jeweils einen komplexen Wert des L1 C/A Codes entgegen
   * 
   * @param real Realteil des komplexen Codes
   * @param imag Imagin�rteil des komplexen Codes
   */
  
  public void enterCode(float real, float imag){
  }
  
  /**
   * F�hrt Akquisition aus
   * Gibt die Akquisitionsnachricht zur�ck
   * Speichert Doppler- und Codeverschiebung intern
   * 
   * @return "True" wenn ein das Signal eines Satelliten empfangen wurde sonst "False"
   */
  
  public boolean startAcquisition(){
    //TODO Xfd[n] = Xin[n]*exp(-(j*2*pi*fd*n/fs))
    XinToXfd.computeXfd(null);
    
    //TODO rfd -> von innen nach Au�en:
    //TODO DFT(Xfd)
    DFT.computeDFT(null);
    
    //TODO DFT(C)
    DFT.computeDFT(null);
    
    //TODO [DFT(C)]*
    KonjugiertMatrix.computeKonjugierteMatrix(null);
    
    //TODO [[DFT(C)]*]T
    TransponiertMatrix.computeTransponierteMatrix(null);
    
    //TODO DFT(Xfd) * [[DFT(C)]*]T
    MatrizenMultiplizierer.computeMatrixMuliplikation(null, null);
    
    //TODO IDFT[ DFT(Xfd) * [[DFT(C)]*]T]
    InverseDFT.computeIDFT(null);
    
    //TODO 1/N * IDFT[ DFT(Xfd) * [[DFT(C)]*]T]
    MatrixKonstanteMultiplizierer.computeMatrixKonstanteMultiplikation(null);
    
    //TODO finde Maximalwert in Ergebnismatrix + speichere Doppler- und Codeverschiebung
    MaximalwertFinder.findMaximalwert(null);
    
    //TODO berechne gesch�tzte Signalleistung
    MatrixMittelwert.computeMatrixMittelwert(null);
    
    //TODO normiere den Maximalwert mit der Signalleistung
    
    //TODO entscheide, ob  normierter Maximalwert hoch genug ist
    
    return false;
  }
  
  /**
   * Auslesen der Dopplerverschiebung
   * 
   * @return Dopplerverschiedung
   */
  
  public int getDopplerverschiebung(){
    return 0;
  }
  
  
  /**
   * Auslesen der Codeverschiebung
   * 
   * @return Codeverschiebung
   */
  
  public int getCodeVerschiebung(){
    return 0;
  }
  
  

}
