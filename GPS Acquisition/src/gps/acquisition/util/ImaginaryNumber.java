package gps.acquisition.util;

public class ImaginaryNumber {
	
	float realPart;
	
	float imaginaryPart;
	
	/**
	 * Initialisiert real und imagin�r Teil mit 0
	 */
	
	public ImaginaryNumber() {
		realPart = 0;
		imaginaryPart = 0;
	}
	
	public ImaginaryNumber(float newRealPart,float newImaginaryPart) {
		realPart = newRealPart;
		imaginaryPart = newImaginaryPart;
	}
	
	public float getRealPart() {
		return realPart;
	}
	
	
	public float getImaginaryPart() {
		return realPart;
	}
	
	public void setRealPart(float newRealPart) {
		realPart = newRealPart;
	}
	
	public void setImaginaryPart(float newImaginaryPart) {
		imaginaryPart = newImaginaryPart;
	}

}
